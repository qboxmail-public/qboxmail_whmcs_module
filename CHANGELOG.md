# Changelog

## v1.0.4 - _July 7th, 2022_

- White Label support improvement
- Documentation improvement
- Added guide for using the module during the Qboxmail Free Trial
- Bug fixing
 

## v1.0.3 - _March 17th, 2022_

- View/hide detailed changelog
