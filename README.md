# Introduction

Qboxmail's module for WHMCS allows you to sell Qboxmail email accounts to your customers directly from your WHMCS. This will make it easier for your customers to bundle a Professional Cloud Email service with their domain or web space. **Qboxmail module for WHMCS is free**, in case of technical problems or suggestions please open a ticket in your Qboxmail customer area.

# Why Qboxmail

**Qboxmail is the complete cloud-based email management solution for hosting providers.**

Increase the value of your service by adding a robust professional email solution to your offering. Qboxmail will provide you with a set of tools that will allow you to expand your business, enriching your offer, in a simple way, without having to worry about all the problems that usually come with managing an email server.

Our Control Panel is specifically designed to save you time in managing your customers' emails, and **with our WHMCS module the creation of email accounts is automatic.**

# Qboxmail features

The features of our service make Qboxmail attractive both to resellers like you, and to your customers.

## For you:

- WHMCS Integration
- Multilevel control panel
- Automatic migration from other email servers
- Real-time email log analytics
- API for software integrations
- White-label solutions for Control Panel and Webmail

## For your customer:

- Robust built-in email security solutions
- Mailboxes up to 100 GB
- Backup and Email Archive with no limited space
- Centralized management of e-mail signatures
- Modern and user-friendly Webmail
- Contacts and calendars sharing
- Zoom integration for video meeting
- Manage multiple email accounts at the same time
- Aliases, forwards, vacation messages, sieve rules, canned messages

# How the Module Works

With the Qboxmail module for WHMCS you can:

Add, remove, enable or disable mailboxes
Update passwords
Managing quota
Add, remove aliases
Create different products as “email packages” in the three different plans of our service

# Where do I start?

Visit [www.qboxmail.com/resellers](https://www.qboxmail.com/resellers/) to receive more information about our **resellers program** and learn about all the advantages of a partnership with us. Then sign up for our 30 days free trial at [www.qboxmail.com/free-trial](https://www.qboxmail.com/free-trial/)

After you signed up it’s time to try our module. Follow our Step-by-Step Guides to easily install the module.

We know the world is full of choices. Thank you for choosing us!
