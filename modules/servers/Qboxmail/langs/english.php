<?php

$_LANG['token']                  = ', Error Token:';
$_LANG['generalError']           = 'Something has gone wrong. Check the logs and contact the administrator.';
$_LANG['generalErrorClientArea'] = 'Something has gone wrong. Contact the administrator.';
$_LANG['permissionsStorage']     = ':storage_path: settings are not sufficient. Please set up permissions to the \'storage\' folder as writable.';
$_LANG['undefinedAction']        = 'Undefined Action';
$_LANG['changesHasBeenSaved']    = 'Changes have been saved successfully';
$_LANG['Monthly']                = 'Monthly';
$_LANG['Free Account']           = 'Free Account';
$_LANG['Passwords']              = 'Passwords';
$_LANG['Nothing to display']     = 'Nothing to display';
$_LANG['Search']                 = 'Search';
$_LANG['Previous']               = 'Previous';
$_LANG['Next']                   = 'Next';
$_LANG['searchPlacecholder']     = 'Search...';

$_LANG['noDataAvalible']                 = 'No Data Available';
$_LANG['datatableItemsSelected']         = 'Items Selected';
$_LANG['validationErrors']['emptyField'] = 'This field cannot be empty';
$_LANG['bootstrapswitch']['disabled']    = 'Disabled';
$_LANG['bootstrapswitch']['enabled']     = 'Enabled';

$_LANG['addonCA']['pageNotFound']['title']       = 'No page has been found';
$_LANG['addonCA']['pageNotFound']['description'] = 'Provided URL does not exist on this page. If you are sure that an error occurred here, please contact support.';
$_LANG['addonCA']['pageNotFound']['button']      = 'Return to the previous page';

$_LANG['addonCA']['errorPage']['title']       = 'AN ERROR OCCURRED';
$_LANG['addonCA']['errorPage']['description'] = 'An error occurred. Please contact administrator and pass the details:';
$_LANG['addonCA']['errorPage']['button']      = 'Return to the previous page';
$_LANG['addonCA']['errorPage']['error']       = 'ERROR';

$_LANG['addonCA']['errorPage']['errorCode']    = 'Error Code';
$_LANG['addonCA']['errorPage']['errorToken']   = 'Error Token';
$_LANG['addonCA']['errorPage']['errorTime']    = 'Time';
$_LANG['addonCA']['errorPage']['errorMessage'] = 'Message';

$_LANG['addonAA']['pageNotFound']['title']       = 'PAGE NOT FOUND';
$_LANG['addonAA']['pageNotFound']['description'] = 'An error occurred. Please contact administrator.';
$_LANG['addonAA']['pageNotFound']['button']      = 'Return to the previous page';

$_LANG['addonAA']['errorPage']['title']       = 'AN ERROR OCCURRED';
$_LANG['addonAA']['errorPage']['description'] = 'An error occurred. Please contact administrator and pass the details:';
$_LANG['addonAA']['errorPage']['button']      = 'Return to the previous page';
$_LANG['addonAA']['errorPage']['error']       = 'ERROR';

$_LANG['addonAA']['errorPage']['errorCode']    = 'Error Code';
$_LANG['addonAA']['errorPage']['errorToken']   = 'Error Token';
$_LANG['addonAA']['errorPage']['errorTime']    = 'Time';
$_LANG['addonAA']['errorPage']['errorMessage'] = 'Message';

/* * ********************************************************************************************************************
 *                                                   ERROR CODES                                                        *
 * ******************************************************************************************************************** */
$_LANG['errorCodeMessage']['Uncategorised error occured']         = 'Unexpected Error';
$_LANG['errorCodeMessage']['Database error']                      = 'Database error';
$_LANG['errorCodeMessage']['Provided controller does not exists'] = 'Page not found';
$_LANG['errorCodeMessage']['Invalid Error Code!']                 = 'Unexpected Error';

/* * ********************************************************************************************************************
 *                                                   MODULE REQUIREMENTS                                                *
 * ******************************************************************************************************************** */
$_LANG['unfulfilledRequirement']['In order for the module to work correctly, please remove the following file: :remove_file_requirement:'] = 'In order for the module to work correctly, please remove the following file: :remove_file_requirement:';

/* * ********************************************************************************************************************
 *                                                   ADMIN AREA                                                         *
 * ******************************************************************************************************************** */

$_LANG['invalidServerType'] = 'Please set up Qboxmail server first.';

$_LANG['addonAA']['datatables']['next']                       = 'Next';
$_LANG['addonAA']['datatables']['previous']                   = 'Previous';
$_LANG['addonAA']['datatables']['zeroRecords']                = 'Nothing to display';
$_LANG['formValidationError']                                 = 'A form validation error has occurred';
$_LANG['FormValidators']['thisFieldCannotBeEmpty']            = 'This field cannot be empty';
$_LANG['FormValidators']['PleaseProvideANumericValue']        = 'Please provide a numeric value';
$_LANG['FormValidators']['PleaseProvideANumericValueBetween'] = 'Please provide a numeric value between :minValue: and :maxValue:';

// -------------------------------------------------> MENU <--------------------------------------------------------- //
$_LANG['changesSaved']                                        = 'Changes Saved Succesfully';
$_LANG['ItemNotFound']                                        = 'Item Not Found';
$_LANG['formValidationError']                                 = 'Form Validation Error';
$_LANG['FormValidators']['thisFieldCannotBeEmpty']            = 'This Field Cannot Be Empty';
$_LANG['FormValidators']['PleaseProvideANumericValue']        = 'Please Provide A Numeric Value';
$_LANG['FormValidators']['PleaseProvideANumericValueBetween'] = 'Please Provide A Numeric Value Between :minValue: AND :maxValue:';

/* * ********************************************************************************************************************
 *                                                    CLIENT AREA                                                      *
 * ******************************************************************************************************************** */

$_LANG['addonCA']['verifyDomain']['title']            = 'Verify Domain Ownership';
$_LANG['addonCA']['verifyDomain']['instruction']      = 'To verify the ownership you need to create the record type A below in the configuration of the DNS of the domain:';
$_LANG['addonCA']['verifyDomain']['record_name']      = 'NAME';
$_LANG['addonCA']['verifyDomain']['record_type']      = 'TYPE';
$_LANG['addonCA']['verifyDomain']['record_ip']        = 'IP / RECORD';
$_LANG['addonCA']['verifyDomain']['additional_info']  = 'Once you have created the record, click on <b>Verify DNS</b> to enable the domain. It could take up to 24 hours until the new configuration will work.';
$_LANG['addonCA']['verifyDomain']['check_dns_button'] = 'Verify Domain DNS';
$_LANG['addonCA']['verifyDomain']['unverified']       = 'Check if you have added the record correctly. If you added the record correctly, wait for it to be checked.';
$_LANG['addonCA']['verifyDomain']['success']          = 'Your domain has been successfully verified. Now you can configure your email services.';

$_LANG['management']                           = 'Management';
$_LANG['emailAccount']                         = 'Email Accounts';
$_LANG['emailAlias']                           = 'Email Aliases';
$_LANG['goToPanel']                            = 'Email Panel';
$_LANG['goToWebmail']                          = 'Webmail';
$_LANG['addonCA']['homePage']['manageHeader']  = 'Email Management';
$_LANG['addonCA']['homeIcons']['emailAccount'] = 'Email Accounts';
$_LANG['addonCA']['homeIcons']['goToPanel']    = 'Email Panel';
$_LANG['addonCA']['homeIcons']['goToWebmail']  = 'Webmail';
$_LANG['addonCA']['homeIcons']['emailAlias']   = 'Email Aliases';

$_LANG['addonCA']['emailAccount']['emailAccountDescription'] = 'Email Accounts Management';
$_LANG['addonCA']['emailAlias']['emailAliasDescription']     = 'Email Aliases Management';

$_LANG['addonCA']['breadcrumbs']['Qboxmail']                                                                     = 'Email Accounts';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['addAccountButton']['button']['addAccountButton'] = 'Add Email Account';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['table']['id']                                    = 'Email ID';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['table']['fullName']                              = 'Full Name';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['table']['email']                                 = 'Email';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['table']['maxQuota']                              = 'Max Quota';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['table']['usedQuota']                             = 'Used Quota';

$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['table']['mailenabled']                                 = 'Status';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['table']['max_quota']                                   = 'Max Quota';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['editAccountButton']['button']['editAccountButton']     = 'Edit Email Account';
$_LANG['addonCA']['emailAccount']['mainContainer']['accounts']['deleteAccountButton']['button']['deleteAccountButton'] = 'Delete Email Account';
$_LANG['addonCA']['emailAccount']['enabledField']['field_enabled_default']                                             = 'Enabled';

$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_0']['generated_0_0']['firstname']['firstname']             = 'First Name';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_0']['generated_0_1']['lastname']['lastname']               = 'Last Name';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['usernameGroup']['usernameGroup']                                                 = 'Primary Email';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_1']['generated_1_0']['password']['password']               = 'Password';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_1']['generated_1_0']['password']['passDescription']        = 'Password for the user Postmaster (min 8 chars and at least one number).';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_1']['generated_1_1']['repeat_password']['repeat_password'] = 'Repeat Password';
$_LANG['addonCA']['emailAccount']['addAccountForm']['generalSection']['generated_row_section_2']['generated_2_0']['max_email_size']['max_email_size']   = 'Max Email Quota';

$_LANG['addonCA']['emailAccount']['addAccountModal']['modal']['addAccountModal']  = 'Add Email Account';
$_LANG['addonCA']['emailAccount']['addAccountModal']['generalSection']            = 'General Settings';
$_LANG['addonCA']['emailAccount']['addAccountModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['emailAccount']['addAccountModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['generated_row_section_0']['generated_0_0']['firstname']['firstname']             = 'First name';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['generated_row_section_0']['generated_0_1']['lastname']['lastname']               = 'Last name';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['usernameGroup']['usernameGroup']                                                 = 'Primary Email';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['generated_row_section_1']['generated_1_0']['password']['password']               = 'Password';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['generated_row_section_1']['generated_1_0']['password']['passDescription']        = 'Password for the user Postmaster (min 8 chars and at least one number). Leave the field blank if you do not want to change the password.';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['generated_row_section_1']['generated_1_1']['repeat_password']['repeat_password'] = 'Repeat Password';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['generated_row_section_2']['generated_2_0']['max_email_size']['max_email_size']   = 'Max Email Quota';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['status']['status']                                                               = 'Status';

$_LANG['addonCA']['emailAccount']['editAccountModal']['modal']['editAccountModal'] = 'Edit Email Account';
$_LANG['addonCA']['emailAccount']['editAccountModal']['editGeneralSection']        = 'General Settings';
$_LANG['addonCA']['emailAccount']['editAccountModal']['baseAcceptButton']['title'] = 'Confirm';
$_LANG['addonCA']['emailAccount']['editAccountModal']['baseCancelButton']['title'] = 'Cancel';

$_LANG['addonCA']['emailAccount']['deleteAccountModal']['modal']['deleteAccountModal']               = 'Delete Email Account';
$_LANG['addonCA']['emailAccount']['deleteAccountModal']['deleteAccountForm']['confirmRemoveAccount'] = 'Are you sure you want to delete this email account?';
$_LANG['addonCA']['emailAccount']['deleteAccountModal']['baseAcceptButton']['title']                 = 'Confirm';
$_LANG['addonCA']['emailAccount']['deleteAccountModal']['baseCancelButton']['title']                 = 'Cancel';

$_LANG['addonCA']['emailAlias']['mainContainer']['aliases']['addEmailAliasButton']['button']['addEmailAliasButton']       = 'Add Email Alias';
$_LANG['addonCA']['emailAlias']['mainContainer']['aliases']['table']['alias']                                             = 'Destinations';
$_LANG['addonCA']['emailAlias']['mainContainer']['aliases']['table']['email']                                             = 'Email';
$_LANG['addonCA']['emailAlias']['mainContainer']['aliases']['deleteEmailAliasButton']['button']['deleteEmailAliasButton'] = 'Delete Email Alias';
$_LANG['addonCA']['emailAlias']['mainContainer']['aliases']['editEmailAliasButton']['button']['editEmailAliasButton']     = 'Edit Email Aliases';

$_LANG['addonCA']['emailAlias']['addEmailAliasModal']['modal']['addEmailAliasModal']                         = 'Add Email Alias';
$_LANG['addonCA']['emailAlias']['addEmailAliasModal']['addEmailAliasForm']['destinations']['destinations']   = 'Destinations';
$_LANG['addonCA']['emailAlias']['addEmailAliasModal']['addEmailAliasForm']['usernameGroup']['usernameGroup'] = 'Primary Email';
$_LANG['addonCA']['emailAlias']['addEmailAliasModal']['baseAcceptButton']['title']                           = 'Confirm';
$_LANG['addonCA']['emailAlias']['addEmailAliasModal']['baseCancelButton']['title']                           = 'Cancel';

$_LANG['addonCA']['emailAlias']['editEmailAliasModal']['editEmailAliasForm']['destinations']['destinations']   = 'Destinations';
$_LANG['addonCA']['emailAlias']['editEmailAliasModal']['editEmailAliasForm']['usernameGroup']['usernameGroup'] = 'Primary Email';
$_LANG['addonCA']['emailAlias']['editEmailAliasModal']['modal']['editEmailAliasModal']                         = 'Edit Email Alias';
$_LANG['addonCA']['emailAlias']['editEmailAliasModal']['deleteAccountForm']['confirmDeleteAccountAlias']       = 'Are you sure you want to edit this email alias?';
$_LANG['addonCA']['emailAlias']['editEmailAliasModal']['baseAcceptButton']['title']                            = 'Confirm';
$_LANG['addonCA']['emailAlias']['editEmailAliasModal']['baseCancelButton']['title']                            = 'Cancel';

$_LANG['addonCA']['emailAlias']['deleteEmailAliasModal']['modal']['deleteEmailAliasModal']                 = 'Delete Email Alias';
$_LANG['addonCA']['emailAlias']['deleteEmailAliasModal']['deleteAccountForm']['confirmDeleteAccountAlias'] = 'Are you sure you want to delete this email alias?';
$_LANG['addonCA']['emailAlias']['deleteEmailAliasModal']['baseAcceptButton']['title']                      = 'Confirm';
$_LANG['addonCA']['emailAlias']['deleteEmailAliasModal']['baseCancelButton']['title']                      = 'Cancel';


//PRODUCT SETTINGS
$_LANG['mainContainer']['configForm']['qboxmailSettings']['qboxmailSettings']                                                      = 'Product Settings';
$_LANG['mainContainer']['configForm']['qboxmailSettings']['leftSide']['max_email_accounts']['max_email_accounts']                  = 'Maximum Email Accounts';
$_LANG['mainContainer']['configForm']['qboxmailSettings']['leftSide']['plan']['plan']                                              = 'Plan';
$_LANG['mainContainer']['configForm']['qboxmailSettings']['leftSide']['max_email_quota']['max_email_quota']                        = 'Max Email Quota';
$_LANG['mainContainer']['configForm']['qboxmailSettings']['rightSide']['max_email_accounts_archive']['max_email_accounts_archive'] = 'Max Email Accounts Archive';
$_LANG['mainContainer']['configForm']['qboxmailSettings']['rightSide']['max_email_accounts_25gb']['max_email_accounts_25gb']       = 'Max Email Accounts 25GB';
$_LANG['mainContainer']['configForm']['qboxmailSettings']['leftSide']['max_email_accounts_50gb']['max_email_accounts_50gb']        = 'Max Email Accounts 50GB';
$_LANG['mainContainer']['configForm']['qboxmailSettings']['rightSide']['max_email_accounts_100gb']['max_email_accounts_100gb']     = 'Max Email Accounts 100GB';

//PLANS
$_LANG['plan']['basic']        = 'Basic';
$_LANG['plan']['professional'] = 'Professional';
$_LANG['plan']['enterprise']   = 'Enterprise';

//CONFIGURABLE OPTIONS
$_LANG['mainContainer']['configurablesOptions']['button']['createCOBaseModalButton']     = 'Create Configurable Options';
$_LANG['configurableOptionsWhmcsInfo']                                                   = 'configurableOptionsWhmcsInfo';
$_LANG['createCOConfirmModal']['createConfigurableForm']['configurableOptionsWhmcsInfo'] = 'Below you can choose which configurable options will be generated for this product. Please note that these options are divided into two parts separated by a | sign, where the part on the left indicates the sent variable and the part on the right the friendly name displayed to customers. After generating these options you can edit the friendly part on the right, but not the variable part on the left. More information about configurable options and their editing can be found <a style="color: #31708f; text-decoration: underline;" href="https://docs.whmcs.com/Addons_and_Configurable_Options" target="blank">here</a>.';
$_LANG['createCOConfirmModal']['modal']['createCOConfirmModal']                          = 'Create Configurable Options';
$_LANG['createCOConfirmModal']['baseAcceptButton']['title']                              = 'Confirm';
$_LANG['createCOConfirmModal']['baseCancelButton']['title']                              = 'Cancel';
//

//EMAIL ACCOUNT STATUSES
$_LANG['account']['status']['enabled']  = 'Enabled';
$_LANG['account']['status']['disabled'] = 'Disabled';
$_LANG['account']['status']['updating'] = 'Updating';
$_LANG['account']['status']['failure']  = 'Failure';


$_LANG['ownDomainNeed'] = 'You must have a domain in WHMCS with the same name to use the product: ';

$_LANG['addonCA']['home']['mainContainer']['dataTable']['serviceInformation'] = 'Service Information';
$_LANG['addonCA']['home']['mainContainer']['dataTable']['table']['name']      = 'Name';
$_LANG['addonCA']['home']['mainContainer']['dataTable']['table']['value']     = 'Value';

$_LANG['addonCA']['home']['status']                                                                      = 'Status';
$_LANG['addonCA']['home']['usedEmailAccs']                                                               = 'Email Accounts';
$_LANG['addonCA']['home']['usedAliases']                                                                 = 'Email Aliases';
$_LANG['addonCA']['home']['enabledField']['field_enabled_danger']                                        = 'Disabled';
$_LANG['addonCA']['home']['enabledField']['field_enabled_success']                                       = 'Enabled';
$_LANG['addonCA']['emailAccount']['enabledField']['field_enabled_success']                               = 'Enabled';
$_LANG['addonCA']['emailAccount']['enabledField']['field_enabled_danger']                                = 'Disabled';
$_LANG['addonCA']['emailAccount']['editAccountForm']['editGeneralSection']['mailenabled']['mailenabled'] = 'Enabled Status';

$_LANG['status']                                           = 'Status';
$_LANG['moduleAccess']                                     = 'Module Access';
$_LANG['usedQuotaData']                                    = 'Quota Usage';
$_LANG['usedEmailAccs']                                    = 'Email Accounts';
$_LANG['usedAliases']                                      = 'Email Aliases';
$_LANG['enabledField']['field_enabled_danger']             = 'Disabled';
$_LANG['enabledField']['field_enabled_success']            = 'Enabled';
$_LANG['field_enabled_success']                            = 'Enabled';
$_LANG['field_enabled_danger']                             = 'Disabled';
$_LANG['mainContainer']['dataTable']['serviceInformation'] = 'Service Information';

$_LANG['APIError']['Number of emails accounts of 25GB exceeded for domain.']         = "Number of emails accounts of 25GB exceeded for domain.";
$_LANG['APIError']['Number of emails accounts of 50GB exceeded for domain.']         = "Number of emails accounts of 25GB exceeded for domain.";
$_LANG['APIError']['Number of emails accounts of 100GB exceeded for domain.']        = "Number of emails accounts of 25GB exceeded for domain.";
$_LANG['APIError']['Email accounts limit exceeded for domain']                       = "Email accounts limit exceeded for domain.";
$_LANG['APIError']['email_address already exists']                                   = 'Email Address already exists.';
$_LANG['APIError']['Cannot Create alias of the email account']                       = 'Cannot Create alias of the email accout.';
$_LANG['APIError']['name Only alphanumeric characters, - and _ and . are permitted'] = 'Name Only alphanumeric characters, - and _ and . are permitted';
$_LANG['emailAccountMaxEmailError']                                                  = "Email accounts limit exceeded for domain";
$_LANG['emailAccountMaxEmail25GbError']                                              = "Number of emails accounts of 25GB exceeded for domain.";
$_LANG['emailAccountMaxEmail50GbError']                                              = "Number of emails accounts of 50GB exceeded for domain.";
$_LANG['emailAccountMaxEmail100GbError']                                             = "Number of emails accounts of 100GB exceeded for domain.";

$_LANG['emailAccountHasBeenCreated'] = 'Email account has been added successfully';
$_LANG['emailAccountErrorCreated']   = 'Cannot create the email account';
$_LANG['emailAccountHasBeenUpdated'] = 'Email account has been updated successfully';
$_LANG['emailAccountHasBeenDeleted'] = 'Email account has been deleted successfully';
$_LANG['emailAliasCreateError']      = 'Cannot Create alias of the email account';
$_LANG['emailAliasEditError']        = 'Cannot edit email aliases';
$_LANG['emailAliasHasBeenEdited']    = 'Email alias has been edited successfully';
$_LANG['emailAliasHasBeenDeleted']   = 'Email alias has been deleted successfully';
$_LANG['emailAliasHasBeenCreated']   = 'Email alias has been created successfully';
$_LANG['emailAliasHasBeenDeleted']   = 'Email alias has been deleted successfully';
$_LANG['emailAliasesLimit']          = 'Cannot create more email aliases, please upgrade the product';
$_LANG['emailAccountsLimit']         = 'Cannot create more email accounts, please upgrade the product';
$_LANG['quotaLimit']                 = 'Cannot create email account with such a large quota';
$_LANG['configurableOptionsCreate']  = 'Configurable Options has been created successfully';
$_LANG['configurableOptionsUpdate']  = 'Configurable Options has been updated successfully';
$_LANG['emailAliasExists']           = 'This email alias already exists';
$_LANG['emailAccountsUpdateError']   = 'Email account cannot be updated';

//VALIDATORS
$_LANG['FormValidators']['passwordCharsLengthError'] = 'Password must contain at least 8 characters and one number.';
$_LANG['FormValidators']['passwordsIsNotTheSame']    = 'Password are not the same';
$_LANG['FormValidators']['tooMuchDestinationsError'] = 'Too Much Destinations';
$_LANG['tooManyAliases']                             = 'You cannot create next email account, because you increased amount of email aliases and next email account will generate new email alias. Please upgrade email aliases amount first';
$_LANG['invalid_accounts_configuration']             = 'The maximum number of accounts is less than the number of 25gb accounts or archive accounts.';


