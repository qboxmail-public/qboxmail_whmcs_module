<?php

namespace ModulesGarden\Servers\Qboxmail\Core\App\Packages;

class PackageConfigurationConst
{
    const VERSION = 'version';
    const PACKAGE_NAME = 'packageName';
}
