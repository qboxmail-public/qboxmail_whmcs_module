<?php

namespace ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances;

use ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\Http\AddonIntegration;
use ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\Http\AdminServicesTabFieldsIntegration;
use ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\Http\ConfigOptionsIntegration;
use ModulesGarden\Servers\Qboxmail\Core\App\Controllers\ResponseResolver;
use ModulesGarden\Servers\Qboxmail\Core\Http\JsonResponse;
use ModulesGarden\Servers\Qboxmail\Core\UI\ViewAjax;
use ModulesGarden\Servers\Qboxmail\Core\UI\ViewIntegrationAddon;
use ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Interfaces\DefaultController;
use ModulesGarden\Servers\Qboxmail\Core\Helper;

/**
 * Class AddonController
 *
 * @package ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances
 */
abstract class AddonController implements DefaultController
{
    use \ModulesGarden\Servers\Qboxmail\Core\Traits\Lang;
    use \ModulesGarden\Servers\Qboxmail\Core\Traits\OutputBuffer;
    use \ModulesGarden\Servers\Qboxmail\Core\Traits\IsAdmin;
    use \ModulesGarden\Servers\Qboxmail\Core\UI\Traits\RequestObjectHandler;
    use \ModulesGarden\Servers\Qboxmail\Core\Traits\ErrorCodesLibrary;
    use \ModulesGarden\Servers\Qboxmail\Core\Traits\AppParams;

    /**
     * @param  null  $params
     *
     * @return mixed|Helper\type
     */
    public function runExecuteProcess($params = null)
    {
        /**
         * load lang context
         */
        $this->loadLangContext();

        /**
         *
         * result of execute
         */
        $resault = $this->execute($params);

        /**
         *
         * return as json result
         */
        if ($resault instanceof JsonResponse) {
            $resolver = new ResponseResolver($resault);

            $resolver->resolve();
        }

        /**
         *
         * check if is callable function & add params
         */
        if ($this->isValidIntegrationCallback($resault)) {
            $this->setAppParam('IntegrationControlerName', $resault[0]);
            $this->setAppParam('IntegrationControlerMethod', $resault[1]);

            //to do catch exceptions
            $resault = Helper\di($resault[0], $resault[1]);
        }

        /**
         *
         * return if view ajax
         */
        if ($resault instanceof ViewAjax) {
            $this->resolveAjax($resault);
        }

        /**
         *  return as view integration addon
         */
        if ( ! $resault instanceof ViewIntegrationAddon) {
            return $resault;
        }

        /**
         *
         */
        if ($resault instanceof JsonResponse) {
            $resolver = new ResponseResolver($resault);

            $resolver->resolve();
        }

        $addonIntegration = $this->getIntegrationControler($params['action']);

        return $addonIntegration->runExecuteProcess($resault);

    }

    /**
     * @param  null  $callback
     *
     * @return bool
     */
    public function isValidIntegrationCallback($callback = null)
    {
        if (is_callable($callback)) {
            return true;
        }

        return false;
    }

    /**
     * @param $resault
     */
    public function resolveAjax($resault)
    {
        $ajaxResponse = $resault->getResponse();

        $resolver = new ResponseResolver($ajaxResponse);

        $resolver->resolve();
    }

    /**
     * @param  null  $action
     *
     * @return HttpController | ConfigOptionsIntegration | AdminServicesTabFieldsIntegration | AddonIntegration
     */
    protected function getIntegrationControler($action = null)
    {
        switch ($action) {
            case 'ConfigOptions':
                return Helper\di(\ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\Http\ConfigOptionsIntegration::class);
                break;
            case 'AdminServicesTabFields':
                return Helper\di(\ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\Http\AdminServicesTabFieldsIntegration::class);
                break;
            default:
                return Helper\di(\ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\Http\AddonIntegration::class);
        }
    }

    /**
     *
     */
    public function loadLangContext()
    {
        $this->loadLang();

        if ($this->getAppParam('IntegrationControlerName')) {
            $parts = explode('\\', $this->getAppParam('IntegrationControlerName'));

            $controller = end($parts);
        } else {
            $parts = explode('\\', get_class($this));

            $controller = end($parts);
        }

        $this->lang->setContext(($this->getAppParam('moduleAppType') . ($this->isAdmin() ? 'AA' : 'CA')), lcfirst($controller));
    }
}
