<?php

namespace ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\Http;

use \ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Interfaces\AdminArea;
use \ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\HttpController;

class AdminPageController extends HttpController implements AdminArea
{

}
