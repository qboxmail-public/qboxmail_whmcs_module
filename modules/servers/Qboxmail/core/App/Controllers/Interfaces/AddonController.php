<?php

namespace ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Interfaces;

interface AddonController
{
    public function execute($params = []);
}
