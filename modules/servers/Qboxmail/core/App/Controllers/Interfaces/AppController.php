<?php

namespace ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Interfaces;

interface AppController
{
    public function getControllerInstanceClass($callerName, $params);
}
