<?php

namespace ModulesGarden\Servers\Qboxmail\Core\App\Requirements\Instances;

use ModulesGarden\Servers\Qboxmail\Core\App\Requirements\RequirementsList;
use ModulesGarden\Servers\Qboxmail\Core\App\Requirements\RequirementInterface;

/**
 * Description of PhpExtensions
 *
 * @author INBSX-37H
 */
abstract class PhpExtensions extends RequirementsList implements RequirementInterface
{
    const EXTENSION_NAME = 'extensionName';

    final public function getHandler()
    {
        return \ModulesGarden\Servers\Qboxmail\Core\App\Requirements\Handlers\PhpExtensions::class;
    }
}
