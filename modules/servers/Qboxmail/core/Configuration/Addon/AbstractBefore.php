<?php

namespace ModulesGarden\Servers\Qboxmail\Core\Configuration\Addon;

/**
 * Description of AbstractBefore
 *
 * @author Rafał Ossowski <rafal.os@modulesgarden.com>
 */
abstract class AbstractBefore
{

    public function __construct()
    {

    }
}
