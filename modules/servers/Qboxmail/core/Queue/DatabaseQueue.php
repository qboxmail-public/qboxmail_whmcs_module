<?php

namespace ModulesGarden\Servers\Qboxmail\Core\Queue;

use Illuminate\Contracts\Queue\Queue;

class DatabaseQueue implements Queue
{
    /**
     * @param          $job
     * @param  string  $data
     * @param  string  $queue
     */
    public function push($job, $data = '', $queue = 'default')
    {
        $model        = new \ModulesGarden\Servers\Qboxmail\Core\Queue\Models\Job();
        $model->job   = $job;
        $model->data  = $data;
        $model->queue = $queue;
        $model->save();
    }

    public function pushRaw($payload, $queue = null, array $options = [])
    {

    }

    public function later($delay, $job, $data = '', $queue = null)
    {

    }

    public function pushOn($queue, $job, $data = '')
    {

    }

    public function laterOn($queue, $delay, $job, $data = '')
    {

    }

    /**
     * @param  string  $queue
     *
     * @return mixed
     */
    public function pop($queue = 'default')
    {
        return \ModulesGarden\Servers\Qboxmail\Core\Queue\Models\Job::where('queue', $queue)
            ->where(function ($query) {
                $query->where('status', '=', '')
                    ->orWhere(function ($query) {
                        $query->where('status', '=', \ModulesGarden\Servers\Qboxmail\Core\Queue\Models\Job::STATUS_ERROR);
                        $query->whereRaw('retry_after < NOW()');
                        $query->where('retry_count', '<', '100');
                    })
                    ->orWhere(function ($query) {
                        $query->where('status', '=', \ModulesGarden\Servers\Qboxmail\Core\Queue\Models\Job::STATUS_WAITING);
                        $query->whereRaw('retry_after < NOW()');
                        $query->where('retry_count', '<', '100');
                    });
            })
            ->first();
    }
}