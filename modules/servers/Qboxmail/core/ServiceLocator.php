<?php

namespace ModulesGarden\Servers\Qboxmail\Core;

/**
 * Class ServiceLocator
 *
 * @package ModulesGarden\Servers\Qboxmail\Core
 * @TODO    remove that class //MM
 */
class ServiceLocator extends \ModulesGarden\Servers\Qboxmail\Core\DependencyInjection\DependencyInjection
{
    /**
     * @var bool
     * @TODO - move it to different class //MM
     */
    public static $isDebug = false;
}
