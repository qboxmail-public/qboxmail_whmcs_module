<?php

namespace ModulesGarden\Servers\Qboxmail\Core\Http\Admin;

use ModulesGarden\Servers\Qboxmail\Core\Http\AbstractController;

class PageNotFound extends AbstractController
{
    public function index()
    {
        $pageControler = new \ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Http\PageNotFound();

        return $pageControler->execute();
    }
}
