<?php

namespace ModulesGarden\Servers\Qboxmail\Core\Http;

/**
 * Description of AbstractController
 *
 * @author Rafał Ossowski <rafal.os@modulesgarden.com>
 */
class AbstractController
{
    use \ModulesGarden\Servers\Qboxmail\Core\Traits\OutputBuffer;
    use \ModulesGarden\Servers\Qboxmail\Core\Traits\IsAdmin;
    use \ModulesGarden\Servers\Qboxmail\Core\UI\Traits\RequestObjectHandler;

    public function __construct()
    {
        $this->loadRequestObj();
    }
}
