<?php

namespace ModulesGarden\Servers\Qboxmail\Core\Traits;

use ModulesGarden\Servers\Qboxmail\Core\ServiceLocator;

trait Lang
{
    /**
     * @var null|\ModulesGarden\Servers\Qboxmail\Core\Lang\Lang
     */
    protected $lang = null;

    public function loadLang()
    {
        if ($this->lang === null) {
            $this->lang = ServiceLocator::call('lang');
        }
    }
}