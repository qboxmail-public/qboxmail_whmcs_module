<?php

return [
    \ModulesGarden\Servers\Qboxmail\Core\Lang\Lang::class,
    \ModulesGarden\Servers\Qboxmail\Core\Events\Dispatcher::class,
    \ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\Addon\Config::class,
    \ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Sidebar\SidebarService::class,
];