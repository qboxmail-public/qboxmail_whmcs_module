<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\Widget\RawDataTable;

/**
 * DataTable Widget without any widget wrapper
 */
class RawDataTable extends \ModulesGarden\Servers\Qboxmail\Core\UI\Widget\DataTable\DataTable
{

}
