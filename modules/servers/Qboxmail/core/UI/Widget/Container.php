<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\Widget;

use ModulesGarden\Servers\Qboxmail\Core\UI\Builder\BaseContainer;

/**
 * Simple Container element
 *
 * @author inbs
 */
class Container extends BaseContainer
{
    protected $name = 'container';
    protected $data = [];
}
