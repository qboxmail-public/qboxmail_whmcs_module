<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms;

use \ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Buttons\ButtonSubmitForm;

/**
 * FormWidgetStandalone controler
 *
 * @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
 */
class FormWidgetStandalone extends BaseStandaloneForm implements \ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\AjaxElementInterface, \ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\FormInterface
{
    protected $id = 'formWidgetStandalone';
    protected $name = 'formWidgetStandalone';

}
