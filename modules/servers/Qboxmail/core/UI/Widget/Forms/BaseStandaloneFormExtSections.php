<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms;

/**
 * BaseForm controler
 *
 * @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
 */
class BaseStandaloneFormExtSections extends BaseStandaloneForm
    implements \ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\AjaxElementInterface,
    \ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\FormInterface
{
    protected $id = 'baseStandaloneFormExtSections';
    protected $name = 'baseStandaloneFormExtSections';

}
