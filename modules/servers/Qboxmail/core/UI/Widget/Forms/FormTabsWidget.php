<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms;

/**
 * BaseForm controler
 *
 * @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
 */
class FormTabsWidget extends BaseStandaloneForm
{
    protected $id = 'formTabsWidget';
    protected $name = 'formTabsWidget';
    protected $title = 'formTabsWidget';
}
