<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields;

/**
 * BaseField controler
 *
 * @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
 */
class Hidden extends BaseField
{
    protected $id = 'hidden';
    protected $name = 'hidden';
}
