<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\AutoSaveFields;

use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\BaseField;

/**
 * Switcher field controler
 *
 * @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
 */
class Switcher extends BaseField
{
    protected $id = 'baseSwitcher';
    protected $name = 'baseSwitcher';
}
