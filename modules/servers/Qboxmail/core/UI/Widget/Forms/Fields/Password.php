<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields;

/**
 * BaseField controler
 *
 * @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
 */
class Password extends BaseField
{
    protected $id = 'password';
    protected $name = 'password';
}
