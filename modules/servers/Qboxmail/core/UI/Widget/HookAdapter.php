<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\Widget;

use ModulesGarden\Servers\Qboxmail\Core\UI\Builder\BaseContainer;

class HookAdapter extends BaseContainer
{
    protected $name = 'hookAdapter';
    protected $data = [];
    protected $adaptId = '';

    public function adapt()
    {
        return $this->adaptId;
    }
}