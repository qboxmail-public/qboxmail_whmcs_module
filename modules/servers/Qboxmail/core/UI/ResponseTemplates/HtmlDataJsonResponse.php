<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\ResponseTemplates;

use \ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ResponseInterface;

/**
 *  Ajax Html Data Response
 */
class HtmlDataJsonResponse extends Response implements ResponseInterface
{
    protected $dataType = 'htmlData';
}
