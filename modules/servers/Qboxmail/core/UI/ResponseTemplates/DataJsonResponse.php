<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\ResponseTemplates;

use \ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ResponseInterface;

/**
 *  Ajax Json Data Response
 */
class DataJsonResponse extends Response implements ResponseInterface
{
    protected $dataType = 'data';
}
