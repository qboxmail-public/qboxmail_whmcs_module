<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\ResponseTemplates;

use \ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ResponseInterface;

/**
 * Ajax Raw Data Json Response
 */
class RawDataJsonResponse extends Response implements ResponseInterface
{
    protected $dataType = 'rawData';
}
