<?php

namespace ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces;

/**
 * Ajax Response Interface
 */
interface ResponseInterface
{
    public function getFormatedResponse();
}
