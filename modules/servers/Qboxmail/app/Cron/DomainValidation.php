<?php
namespace ModulesGarden\Servers\Qboxmail\App\Cron;

use ModulesGarden\Servers\Qboxmail\App\Libs\EmailSender;
use ModulesGarden\Servers\Qboxmail\Core\CommandLine\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 13.11.19
 * Time: 09:49
 * Class Migration
 */
class DomainValidation extends Command
{
    /**
     * Command name
     *
     * @var string
     */
    protected $name = 'DomainValidation';

    /**
     * Command description
     *
     * @var string
     */
    protected $description = 'Domain Validation';

    /**
     * Command help text
     *
     * @var string
     */
    protected $help = '';


    protected function process(InputInterface $input, OutputInterface $output, SymfonyStyle $io)
    {
        $io->title('Domain Validation task: Starting');
        $emailSender = new EmailSender();
        $emailSender->scanDomains();
        $io->success("Domain Validation has been finished successfully.");
    }


}
