<?php

namespace ModulesGarden\Servers\Qboxmail\App\Libs;


class Generators
{
    public static function password($len = 8)
    {
        $sets   = [];
        $sets[] = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        $sets[] = '23456789';
        $sets[] = '~!@#$%^&*(){}[],./?';

        $password = '';

        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
        }

        while (strlen($password) < $len) {
            $randomSet = $sets[array_rand($sets)];
            $password  .= $randomSet[array_rand(str_split($randomSet))];
        }

        return str_shuffle($password);
    }
}
