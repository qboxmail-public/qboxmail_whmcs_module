<?php

namespace ModulesGarden\Servers\Qboxmail\App\Libs;

use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\Hosting;
use WHMCS\Database\Capsule as DB;

class EmailTemplate
{
    const NAME_VALID = 'Qboxmail Domains Valid';
    const NAME_INVALID = 'Qboxmail Domains Invalid';

    public static function insertIfNotExists()
    {
        if ( ! self::templateExist(self::NAME_INVALID)) {
            DB::table('tblemailtemplates')->updateOrInsert(
                ['name' => self::NAME_INVALID],
                [
                    'subject' => 'Qboxmail Domain awaiting verification',
                    'type'    => 'product',
                    'custom'  => 1,
                    'message' => '
                              <p>Dear {$client_name}</p>              
                              <p>Your domain {$service_domain} is still not verified. Check that you have correctly configured the domain according to the instructions.</p>
                              <p>Check if you have added the following record correctly:</p>
                              <p>Name: {$domain_code}.{$service_domain}</p>
                              <p>Type: A</p>
                              <p>IP/RECORD: {$ip_address}</p>
                              <p>{$signature}</p>    
                              ',
                ]
            );
        }

        if ( ! self::templateExist(self::NAME_VALID)) {
            DB::table('tblemailtemplates')->updateOrInsert(
                ['name' => self::NAME_VALID],
                [
                    'subject' => 'Qboxmail Domain successfully verified',
                    'type'    => 'product',
                    'custom'  => 1,
                    'message' => '
                              <p>Dear {$client_name}</p>              
                              <p>Your domain ({$service_domain}) has been verified. From now on, you can start using Qboxmail services.</p>
                              <p>{$signature}</p>    
                              ',
                ]
            );
        }


    }

    private static function templateExist($name)
    {
        $template = DB::table('tblemailtemplates')->where('name', $name)->first();
        if ($template) {
            return true;
        }

        return false;
    }


    public static function sendDomainEmailValid($data)
    {
        $data['messagename'] = self::NAME_VALID;
        localAPI('sendEmail', $data);
    }

    public static function sendDomainEmailInvalid($id)
    {
        $hosting   = Hosting::where('id', $id)->first();
        $domainId  = CustomFieldRepository::getCustomFieldValueByServiceId($id, CustomFieldRepository::QBOXMAIL_DOMAIN_ID);
        $ipAddress = (new \ModulesGarden\Servers\Qboxmail\Core\Configuration\Data)->ipAddress;

        $data                = ['id' => $id, 'customvars' => ['domain_code' => $domainId, 'ip_address' => $ipAddress]];
        $data['messagename'] = self::NAME_INVALID;
        localAPI('sendEmail', $data);
    }
}