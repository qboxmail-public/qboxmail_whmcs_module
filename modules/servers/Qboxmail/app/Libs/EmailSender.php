<?php

namespace ModulesGarden\Servers\Qboxmail\App\Libs;


use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use Exception;
use ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\Hosting;
use WHMCS\Carbon;

class EmailSender
{
    private $hostingsData;
    private $api;

    public function __construct()
    {
        $this->hostingsData = $this->getQboxmailHostings();
        $this->api          = new QboxmailApi();
    }

    public function getQboxmailHostings()
    {
        $qboxmailHostings = Hosting::join('tblproducts', 'tblproducts.id', 'tblhosting.packageid')
            ->join('tblorders', 'tblorders.id', 'tblhosting.orderid')
            ->where('tblproducts.servertype', 'Qboxmail')
            ->where('tblhosting.domainstatus', 'Active')
            ->select('tblhosting.id as id', 'tblhosting.domain as domain', 'tblhosting.userid as clientid', 'tblorders.date as orderdate',
                'tblproducts.id as productid')
            ->get();


        return $qboxmailHostings;
    }

    public function scanDomains()
    {
        foreach ($this->hostingsData as $hostingData) {
            $hid     = $hostingData->id;
            $checked = $this->isDomainChecked($hid);
            if ($checked) {
                continue;
            }

            try {
                $this->api->validateDnsOwnership($hid);

                if ($this->compareOrderDateWithNow($hostingData->orderdate)) {
                    EmailTemplate::sendDomainEmailInvalid($hostingData->id);
                }
            } catch (Exception $e) {
                //DOMAIN VALID
                if ($e->getCode() === 404) {
                    EmailTemplate::sendDomainEmailValid(['id' => $hostingData->id]);
                    CustomFieldRepository::saveCustomFieldValue($hid, $hostingData->productid, 'on', CustomFieldRepository::QBOXMAIL_DOMAIN_CHECKED);
                }
            }
        }
    }

    private function isDomainChecked($serviceId)
    {
        $checked = CustomFieldRepository::getCustomFieldValueByServiceId($serviceId, CustomFieldRepository::QBOXMAIL_DOMAIN_CHECKED);
        if ($checked == 'on') {
            return true;
        }

        return false;
    }

    private function compareOrderDateWithNow($orderDate)
    {
        $availableDifferences = [1, 3, 7];

        $now         = Carbon::now();
        $now->hour   = 0;
        $now->minute = 0;
        $now->second = 0;

        $orderDate         = Carbon::parse($orderDate);
        $orderDate->hour   = 0;
        $orderDate->minute = 0;
        $orderDate->second = 0;


        $diffInDays = $now->diffInDays($orderDate);

        if (in_array($diffInDays, $availableDifferences)) {
            return true;
        }

        return false;
    }

}
