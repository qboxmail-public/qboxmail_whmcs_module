<?php

namespace ModulesGarden\Servers\Qboxmail\App\Libs\API;

use \WHMCS\Database\Capsule as DB;
use Exception;

abstract class BaseQboxmailApi
{
    private $uri;
    private $apiToken;

    public function __construct($apiToken = null, $hostname = null)
    {
        if ( ! $hostname) {
            $hostname = DB::table('tblservers')->where('type', 'Qboxmail')->where('active', 1)->value('hostname');
        }

        $hostname  = trim($hostname);
        $this->uri = "https://$hostname/api";

        if ( ! $apiToken) {
            $this->apiToken = DB::table('tblservers')->where('type', 'Qboxmail')->where('active', 1)->value('accesshash');
        } else {
            $this->apiToken = $apiToken;
        }
    }


    protected function request($site, $request, $data = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$this->uri/$site");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request);

        if ($data) {
            curl_setopt(
                $ch,
                CURLOPT_POSTFIELDS,
                json_encode($data)
            );
        }

        $headers = [
            "Content-Type: application/json",
            "Accept: application/json",
            "X-Api-Token: $this->apiToken",
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $raw    = curl_exec($ch);
        $result = json_decode($raw);
        $info   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        logModuleCall('Qboxmail API ' . $request . " code: $info", "$this->uri/$site", ['data' => $data, 'headers' => $headers], $raw, $raw);


        if ($info == 0) {
            throw new Exception('Invalid hostname or api token.');
        } elseif ($info == 401) {
            throw new Exception("Invalid api token, check the token or generate a new one.");
        } elseif ($info == 415) {
            throw new Exception('Unsupported media type', 415);
        } elseif ($info >= 400 && $result) {
            if (isset($result->errors)) {
                $s = "";
                foreach ($result->errors as $error) {
                    $s .= " $error->field $error->description,";
                }
                $s = substr($s, 0, -1);
                throw new Exception($s, $info);
            }
            if (isset($result->message)) {
                throw new Exception($result->message, $info);
            } else {
                throw new Exception($result->error, $info);
            }
        } elseif ($info == 404) {
            throw new Exception("API not found.", $info);
        } elseif ($info > 400) {
            throw new Exception('Something went wrong with Qboxmail API', 500);
        }

        return $result;
    }
}