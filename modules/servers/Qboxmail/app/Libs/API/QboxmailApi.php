<?php

namespace ModulesGarden\Servers\Qboxmail\App\Libs\API;


use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\Hosting;

class QboxmailApi extends BaseQboxmailApi
{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';

    const DOMAIN_ENABLED = 'enabled';
    const DOMAIN_DISABLED = 'disabled';

    public function __construct($apiToken = null, $hostname = null)
    {
        parent::__construct($apiToken, $hostname);
    }

    public function testConnection()
    {
        return $this->request('domains', self::GET);
    }

    public function addDomain($data = [])
    {
        return $this->request('domains', self::POST, $data);
    }

    public function getDomain($serviceId)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}", self::GET);
    }

    public function editDomain($serviceId, $data)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}", self::PUT, $data);
    }

    public function deleteDomain($serviceId)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}", self::DELETE);
    }

    public function enableDomain($serviceId)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/status", self::PUT, ['status' => self::DOMAIN_ENABLED]);
    }

    public function disableDomain($serviceId)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/status", self::PUT, ['status' => self::DOMAIN_DISABLED]);
    }

    public function validateDnsOwnership($serviceId)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/dns_ownership_check", self::PUT);
    }

    public function getEmails($serviceId)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/email_accounts?per=10000", self::GET);
    }

    public function getEmail($serviceId, $emailCode)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/email_accounts/$emailCode", self::GET);
    }

    public function createEmail($serviceId, $data)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/email_accounts", self::POST, $data);
    }

    public function editEmail($serviceId, $emailCode, $data)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/email_accounts/$emailCode", self::PUT, $data);
    }

    public function editEmailStatus($serviceId, $emailCode, $status)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/email_accounts/$emailCode/status", self::PUT, ['status' => $status]);
    }

    public function deleteEmail($serviceId, $emailCode)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/email_accounts/$emailCode", self::DELETE);
    }

    public function getEmailAliases($serviceId)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/alias_email_accounts?per=10000", self::GET);
    }

    public function getEmailAlias($serviceId, $aliasCode)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/alias_email_accounts/$aliasCode", self::GET);
    }

    public function addEmailAlias($serviceId, $data)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/alias_email_accounts", self::POST, $data);
    }

    public function editEmailAlias($serviceId, $aliasCode, $data)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/alias_email_accounts/$aliasCode", self::PUT, $data);
    }

    public function deleteEmailAlias($serviceId, $aliasCode)
    {
        return $this->request("domains/{$this->getDomainCode($serviceId)}/alias_email_accounts/$aliasCode", self::DELETE);
    }

    public function postmasterSSO($serviceId)
    {
        return $this->request("users/{$this->getDomainCode($serviceId)}/postmaster", self::POST);
    }

    private function getDomainCode($serviceId)
    {
        $this->checkServiceId($serviceId);

        return CustomFieldRepository::getCustomFieldValueByServiceId($serviceId, CustomFieldRepository::QBOXMAIL_DOMAIN_ID);
    }

    private function checkServiceId($serviceId)
    {
        if (defined("CLIENTAREA")) {
            $service = Hosting::find($serviceId);

            if ($service->userid !== $_SESSION['uid']) {
                throw new \Exception('Invalid Service ID');
            }
        }
    }
}