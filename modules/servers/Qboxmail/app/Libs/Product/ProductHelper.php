<?php
namespace ModulesGarden\Servers\Qboxmail\App\Libs\Product;


class ProductHelper
{
    public static function bytesToGib($data, $precision = 0)
    {
        return round($data / 1024 / 1024 / 1024, $precision);
    }

    public static function GibToBytes($data)
    {
        return $data * 1024 * 1024 * 1024;
    }
}