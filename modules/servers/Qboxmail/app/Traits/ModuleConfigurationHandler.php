<?php
/**
 * Class ModuleConfigurationHandler
 * User: Nessandro
 * Date: 2019-11-19
 * Time: 08:21
 *
 * @package ModulesGarden\Servers\Qboxmail\App\Traits
 */

namespace ModulesGarden\Servers\Qboxmail\App\Traits;


use ModulesGarden\Servers\Qboxmail\Core\Configuration\Data;

trait ModuleConfigurationHandler
{
    /**
     *
     * @var Data
     */
    protected $moduleData;

    /**
     *
     * @return Data
     */
    public function getModuleData()
    {
        return $this->moduleData;
    }

    /**
     *
     * @param  bool  $reload
     *
     * @return Data|null
     */
    public function loadModuleData($reload = false)
    {
        /**
         * unset data if need reload
         */
        if ($reload) {
            $this->moduleData = null;
        }

        /**
         * load data
         */
        if ( ! $this->moduleData) {
            $this->moduleData = new Data();
        }

        /**
         * return data
         */
        return $this->moduleData;
    }

}