<?php

namespace ModulesGarden\Servers\Qboxmail\App\Traits;


use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Sections\RowSection;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\BaseForm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\HalfPageSection;

trait FormExtendedTrait
{
    protected $sectionCounter = 0;

    /**
     * auto generate double section helper
     *
     * @param  array  $fields
     *
     * @return $this
     * @throws \Exception
     */
    protected function generateDoubleSection($fields = [])
    {
        if ( ! method_exists($this, 'addSection')) {
            throw new \Exception('This trait can not be used in class: ' . self::class);
        }

        $sections     = [];
        $fieldCounter = 0;
        foreach ($fields as $field) {
            $halfPageSection = new HalfPageSection('generated_' . $this->sectionCounter . '_' . $fieldCounter);
            $halfPageSection->addField($field);
            $sections[] = $halfPageSection;

            $fieldCounter++;
        }

        $rawSection = new RowSection('generated_row_section_' . $this->sectionCounter);

        foreach ($sections as $section) {
            $rawSection->addSection($section);
        }

        $this->addSection($rawSection);
        $this->sectionCounter++;

        return $this;
    }

}
