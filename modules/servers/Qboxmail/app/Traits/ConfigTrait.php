<?php

namespace ModulesGarden\Servers\Qboxmail\App\Traits;


/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 11.09.19
 * Time: 14:24
 * Class ConfigTrait
 */
trait ConfigTrait
{
    /**
     * @var array
     */
    private $config = [];

    /**
     * @description set configuration value
     *
     * @param        $name
     * @param  null  $value
     *
     * @return mixed
     */
    public function set($name, $value = null)
    {
        $this->config[$name] = $value;

        return $this;
    }

    /**
     * @description get configuration value
     *
     * @param        $name
     * @param  null  $default
     *
     * @return mixed
     */
    public function get($name, $default = null)
    {
        return isset($this->config[$name]) ? $this->config[$name] : $default;
    }

    /**
     * @description update value
     *
     * @param        $name
     * @param  null  $value
     *
     * @return mixed
     */
    public function update($name, $value = null)
    {
        $this->config[$name] = $value;

        return $this;
    }

    /**
     * @description remove parameter
     *
     * @param $name
     *
     * @return mixed
     */
    public function remove($name)
    {
        unset($this->config[$name]);

        return $this;
    }

    /**
     * @description check if configuration exists
     *
     * @param $name
     *
     * @return bool
     */
    public function exists($name)
    {
        return isset($this->config[$name]) ? true : false;
    }


}