<?php

namespace ModulesGarden\Servers\Qboxmail\App\Traits;


/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 10.09.19
 * Time: 10:47
 * Class ProductService
 */
trait ProductService
{

    /**
     * @var
     */
    protected $productId;

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param  int  $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

}
