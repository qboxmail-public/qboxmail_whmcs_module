<?php

namespace ModulesGarden\Servers\Qboxmail\App\Traits;


use ModulesGarden\Servers\Qboxmail\App\Libs\Restrictions\Restriction;
use ModulesGarden\Servers\Qboxmail\App\Libs\Restrictions\Rules\ExtensionsValid;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 07.11.19
 * Time: 09:54
 * Class ExtensionsCheckerTrait
 */
trait ExtensionsCheckerTrait
{

    /**
     *
     * check about module extensions
     *
     * @throws \Exception
     */
    public function checkExtensionOrThrowError()
    {
        $restriction = new Restriction(new ExtensionsValid());
        $restriction->enableThrowError();
        $restriction->check();
    }

}