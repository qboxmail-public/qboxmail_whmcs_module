<?php

namespace ModulesGarden\Servers\Qboxmail\App\Models;

use \Illuminate\Database\Eloquent\model as EloquentModel;
use ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\ProductConfigOption;

/**
 * Description of ProductConfigGroups
 *
 * @author Mateusz Pawłowski <mateusz.pa@moduelsgarden.com>
 *
 * @property int    $id
 * @property string $name
 * @property string $description
 */
class ProductConfigGroups extends EloquentModel
{

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'tblproductconfiggroups';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function options()
    {
        return $this->hasMany(ProductConfigOption::class, 'gid', 'id');
    }

}
