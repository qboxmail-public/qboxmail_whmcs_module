<?php

namespace ModulesGarden\Servers\Qboxmail\App\Models;

use WHMCS\CustomField\CustomFieldValue;

class CustomFieldValueModel extends CustomFieldValue
{
    protected $fillable = ['fieldid', 'relid', 'value'];
}
