<?php
namespace ModulesGarden\Servers\Qboxmail\App\Models;


/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 09.09.19
 * Time: 11:07
 * Class ProductConfiguration
 */
class ProductConfiguration extends \ModulesGarden\Servers\Qboxmail\Core\Models\ExtendedEloquentModel
{
    protected $table = 'product_configuration';

    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    protected $fillable = ['product_id', 'setting', 'value'];

    protected $softDelete = false;

    public $timestamps = false;

    public static function config($pid)
    {
        $configuration = self::where('product_id', $pid)->get();
        foreach ($configuration as $cModel) {
            $config[$cModel->setting] = $cModel->value;
        }

        return $config;
    }

}