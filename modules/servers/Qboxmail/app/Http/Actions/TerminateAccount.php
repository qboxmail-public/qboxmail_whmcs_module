<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Actions;

use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\App\Libs\OXContext;
use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\AddonController;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 09.09.19
 * Time: 15:14
 * Class SuspendAccount
 */
class TerminateAccount extends AddonController
{

    /**
     * create domain in zimbra
     *
     * @param  null  $params
     *
     * @return string
     */
    public function execute($params = null)
    {
        $productId = $params['packageid'];
        $serviceId = $params['serviceid'];

        try {
            $api = new QboxmailApi();
            $api->deleteDomain($serviceId);

            CustomFieldRepository::saveCustomFieldValue($productId, $productId, '', CustomFieldRepository::QBOXMAIL_DOMAIN_ID);
            CustomFieldRepository::saveCustomFieldValue($serviceId, $productId, '', CustomFieldRepository::QBOXMAIL_PASSWORD);

        } catch (\Exception $ex) {
            return $ex->getMessage();
        }

        return 'success';

    }
}
