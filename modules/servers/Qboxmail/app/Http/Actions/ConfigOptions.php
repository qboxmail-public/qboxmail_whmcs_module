<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Actions;

use ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\Server;
use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\App\Libs\EmailTemplate;
use function ModulesGarden\Servers\Qboxmail\Core\Helper\di;
use ModulesGarden\Servers\Qboxmail\Core\Traits\OutputBuffer;
use ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\Product;
use ModulesGarden\Servers\Qboxmail\Core\App\Installer\ModuleInstaller;
use ModulesGarden\Servers\Qboxmail\App\Http\Admin\ProductConfiguration;
use ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\AddonController;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Pages\ConfigForm;
use ModulesGarden\Servers\Qboxmail\Core\Http\JsonResponse;
use ModulesGarden\Servers\Qboxmail\Core\ServiceLocator;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 28.08.19
 * Time: 09:07
 * Class ConfigOptions
 */
class ConfigOptions extends AddonController
{
    use OutputBuffer;

    /**
     * @param  null  $params
     *
     * @return array
     * @throws \Exception
     */
    public function execute($params = null)
    {
        try {
            if ( ! $this->isCorrectServerType()) {
                return $this->getInvalidServerTypeError();
            }
            $this->updateProductType();

            if (($this->getRequestValue('action') === 'module-settings' || ($this->getRequestValue('loadData') && $this->getRequestValue('ajax') == '1'))) {
                try {
                    //run installer
                    $installer = new ModuleInstaller();
                    $installer->makeInstall();
                    EmailTemplate::insertIfNotExists();
                    CustomFieldRepository::createDomainChecked($_REQUEST['id']);
                    CustomFieldRepository::createPasswordField($_REQUEST['id']);
                    CustomFieldRepository::createDomainId($_REQUEST['id']);
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }
            }
            if ($this->getRequestValue('action') === 'module-settings' && ! $this->getRequestValue('loadData')) {
                if (empty($this->getRequestValue('magic'))) {
                    return [
                        "configoption" => [
                            "Type"        => "",
                            "Description" => $this->getJsCode(),
                        ],
                    ];
                }

                if (isset($_REQUEST['magic'])) {
                    $this->cleanOutputBuffer();

                    return [ProductConfiguration::class, 'index'];
                }

                return [ProductConfiguration::class, 'index'];
            } elseif ($this->getRequestValue('action') === 'save') {
                $form = new ConfigForm();
                $form->runInitContentProcess();
                $form->returnAjaxData();
            } elseif ($this->getRequestValue('ajax') == '1') {
                return [ProductConfiguration::class, 'index'];
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * check if server is configured properly
     *
     * @throws \Exception
     */
    protected function throwErrorIfServerInvalid()
    {
        $productId = di('request')->get('id');
        $product   = Product::where('id', $productId)->first();
        if ( ! $product->servergroup) {
            throw new \Exception(di('lang')->absoluteT('error', 'invalidServer'));
        }
    }

    public function updateProductType()
    {
        $pid         = $this->getRequestValue('id', false);
        $servergroup = $this->getRequestValue('servergroup', 0);

        if ($pid && $servergroup > 0) {
            $product = new \ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\Product();
            $product->where('id', $pid)->update(['servertype' => 'Qboxmail', 'servergroup' => $servergroup]);
        }
    }

    public function isCorrectServerType()
    {
        try {
            if (class_exists('\ModulesGarden\Servers\Qboxmail\App\Http\Actions\MetaData')) {
                $metaDataController = new \ModulesGarden\Servers\Qboxmail\App\Http\Actions\MetaData();
                $details            = $metaDataController->execute(null);
                if ($details['RequiresServer'] !== true) {
                    return true;
                }

                $serverGroupId = $this->getServerGroupId();

                $sModel = new Server();
                $server = $sModel
                    ->select(['tblservers.type'])
                    ->join('tblservergroupsrel', 'tblservergroupsrel.serverid', '=', 'tblservers.id')
                    ->where('tblservergroupsrel.groupid', $serverGroupId)->first();

                if ( ! $server) {
                    return false;
                }

                if ($server->type !== 'Qboxmail') {
                    return false;
                }
            }
        } catch (\Exception $exception) {
            //todo log me
            return false;
        }

        return true;
    }

    public function getServerGroupId()
    {
        $gid = $this->getRequestValue('servergroup', false);
        if ( ! $gid && $gid !== '0' && $gid !== 0) {
            $pid          = $this->getRequestValue('id', 0);
            $productModel = new \ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\Product();
            $product      = $productModel->where('id', $pid)->first();
            if ( ! $product) {
                //can add first product here if needed
                return 0;
            }

            return $product->servergroup;
        }

        return (int)$gid;
    }

    public function getInvalidServerTypeError()
    {
        $lang = ServiceLocator::call('lang');

        $messaage = $lang->addReplacementConstant('provisioning_name', 'Qboxmail')->absoluteT('invalidServerType');
        $data     = $this->buildErrorMessage($messaage);

        return $this->returnAjaxResponse($data);
    }

    public function buildErrorMessage($message)
    {
        $data = [
            'content' =>
                '<div style="width=100%; margin-bottom: 0px;" class="alert alert-danger">' . $message . '</div>',
            'mode'    => 'advanced',
        ];

        return $data;
    }

    public function returnAjaxResponse($data = [])
    {
        $response = new JsonResponse();
        $response->setData($data);

        return $response;
    }

    private function getJsCode()
    {
        $params    = array_merge($this->request->request->all(), $this->request->query->all());
        $dataQuery = http_build_query($params);

        return "
                <script>
                    $('#layers').remove();
                    $('.lu-alert').remove();
                    $('#tblModuleSettings').addClass('hidden');
                    $('#tblMetricSettings').before('<img style=\"margin-left: 50%; margin-top: 15px; margin-bottom: 15px; height: 20px\" id=\"mg-configoptionLoader\" src=\"images/loading.gif\">');
                    $.post({
                        url: '{$_SERVER['HTTP_ORIGIN']}{$_SERVER['PHP_SELF']}?$dataQuery&magic=1'
                    })
                    .done(function( data ){
                        
                        var json = JSON.parse(data);
                       
                        $('#mg-configoptionLoader').remove();
                        if ({$this->getRequestValue('servergroup')} == 0)
                        {
                              $('#tblModuleSettings').html(json.content).removeClass('hidden');
                        }
                        else
                        {
                            $('#tblModuleSettings').html(json.content).removeClass('hidden');
                        }
//                        
                    });
                </script>";
    }

}
