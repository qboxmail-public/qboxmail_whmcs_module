<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Actions;

use Exception;
use ModulesGarden\Servers\Qboxmail\App\Enums\Qboxmail;
use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\App\Libs\Product\ProductHelper;
use ModulesGarden\Servers\Qboxmail\App\Models\ProductConfiguration;
use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\AddonController;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 09.09.19
 * Time: 15:14
 * Class CreateAccount
 */
class CreateAccount extends AddonController
{

    public function execute($params = null)
    {
        try {
            $data          = [];
            $configOptions = $params['configoptions'];
            $productId     = $params['packageid'];
            $serviceId     = $params['serviceid'];
            $config        = ProductConfiguration::config($productId);

            if ($config[Qboxmail::PLAN] === Qboxmail::BASIC) {
                unset($configOptions[Qboxmail::EMAIL_ACCOUNTS_100GB]);
                unset($configOptions[Qboxmail::EMAIL_ACCOUNTS_50GB]);
            } elseif ($configOptions[Qboxmail::PLAN] === Qboxmail::PROFESSIONAL) {
                unset($configOptions[Qboxmail::EMAIL_ACCOUNTS_100GB]);
            }

            $maxEmailAccounts = 0;

            foreach ($configOptions as $option) {
                $maxEmailAccounts += $option;
            }

            $data[Qboxmail::MAX_EMAIL_ACCOUNTS_25GB] = $configOptions[Qboxmail::EMAIL_ACCOUNTS_25GB];

            if ($config[Qboxmail::PLAN] === Qboxmail::BASIC) {
                $data[Qboxmail::MAX_EMAIL_QUOTA] = ProductHelper::GibToBytes(25);
            } elseif ($config[Qboxmail::PLAN] === Qboxmail::PROFESSIONAL) {
                $data[Qboxmail::MAX_EMAIL_QUOTA]         = ProductHelper::GibToBytes(50);
                $data[Qboxmail::MAX_EMAIL_ACCOUNTS_50GB] = $configOptions[Qboxmail::EMAIL_ACCOUNTS_50GB];
            } elseif ($config[Qboxmail::PLAN] === Qboxmail::ENTERPRISE) {
                $data[Qboxmail::MAX_EMAIL_QUOTA]          = ProductHelper::GibToBytes(100);
                $data[Qboxmail::MAX_EMAIL_ACCOUNTS_50GB]  = $configOptions[Qboxmail::EMAIL_ACCOUNTS_50GB];
                $data[Qboxmail::MAX_EMAIL_ACCOUNTS_100GB] = $configOptions[Qboxmail::EMAIL_ACCOUNTS_100GB];
            }

            $data[Qboxmail::MAX_EMAIL_ACCOUNTS] = $maxEmailAccounts;
            $data[Qboxmail::PLAN]               = $config[Qboxmail::PLAN];

            $password = CustomFieldRepository::getCustomFieldValueByServiceId($serviceId, CustomFieldRepository::QBOXMAIL_PASSWORD);
            $password = decrypt($password);

            $data['name']                             = $params['domain'];
            $data['postmaster_password']              = $password;
            $data['postmaster_password_confirmation'] = $password;
            $api                                      = new QboxmailApi();
            $res                                      = $api->addDomain($data);

            CustomFieldRepository::saveCustomFieldValue($serviceId, $productId, $res->resource_code, CustomFieldRepository::QBOXMAIL_DOMAIN_ID);
            CustomFieldRepository::saveCustomFieldValue($serviceId, $productId, '', CustomFieldRepository::QBOXMAIL_DOMAIN_CHECKED);
        } catch (Exception $ex) {
            /**
             * return some crit error
             */
            return $ex->getMessage();
        }

        return 'success';

    }
}
