<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Actions;

use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\App\Libs\OXContext;
use ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\AddonController;
use ModulesGarden\Servers\Qboxmail\Core\HandlerError\Exceptions\Exception;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 09.09.19
 * Time: 15:14
 * Class SuspendAccount
 */
class SuspendAccount extends AddonController
{

    /**
     * create domain in zimbra
     *
     * @param  null  $params
     *
     * @return string
     */
    public function execute($params = null)
    {
        $productId = $params['serviceid'];

        try {
            $api = new QboxmailApi();
            $api->disableDomain($productId);
        } catch (\Exception $ex) {
            /**
             * return some crit error
             */
            return $ex->getMessage();
        }

        return 'success';

    }
}
