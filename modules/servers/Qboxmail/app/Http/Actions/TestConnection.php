<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Actions;

use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\AddonController;


class TestConnection extends AddonController
{
    public function execute($params = null)
    {
        try {
            $api = new QboxmailApi($params['serveraccesshash'], $params['serverhostname']);

            $api->testConnection();
        } catch (\Exception $ex) {
            return ['error' => $ex->getMessage()];
        }

        return ['success' => true];

    }
}
