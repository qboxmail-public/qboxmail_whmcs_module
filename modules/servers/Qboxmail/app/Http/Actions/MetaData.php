<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Actions;

use \ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Instances\AddonController;

/**
 * Class MetaData
 *
 * @author <slawomir@modulesgarden.com>
 */
class MetaData extends AddonController
{
    public function execute($params = null)
    {
        return [
            'DisplayName'    => 'Qboxmail',
            'RequiresServer' => true,
        ];
    }
}
