<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Admin;

use ModulesGarden\Servers\Qboxmail\Core\Helper;
use ModulesGarden\Servers\Qboxmail\Core\UI\Traits\WhmcsParams;
use ModulesGarden\Servers\Qboxmail\Core\Http\AbstractClientController;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Home\Pages\ServiceInformation as ServiceInformationIntegration;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Pages\ConfigForm;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Pages\ConfigurablesOptions;


/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 28.08.19
 * Time: 09:10
 * Class ProductConfiguration
 */
class ServiceInformation extends AbstractClientController
{
    use WhmcsParams;

    public function index()
    {


    }

}