<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Admin;

use ModulesGarden\Servers\Qboxmail\Core\Helper;
use ModulesGarden\Servers\Qboxmail\Core\Http\AbstractClientController;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Home\Pages\ServiceInformation;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Pages\ConfigForm;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Pages\ConfigurablesOptions;


/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 28.08.19
 * Time: 09:10
 * Class ProductConfiguration
 */
class ProductConfiguration extends AbstractClientController
{

    public function index()
    {
        $view = Helper\viewIntegrationAddon()
            ->addElement(ConfigForm::class)
            ->addElement(ConfigurablesOptions::class);

        return $view;
    }

}