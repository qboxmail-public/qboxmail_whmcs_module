<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Client;

use ModulesGarden\Servers\Qboxmail\Core\Http\AbstractClientController;
use ModulesGarden\Servers\Qboxmail\Core\Helper;

class Webmail extends AbstractClientController
{
    public function index()
    {
        $link = (new \ModulesGarden\Servers\Qboxmail\Core\Configuration\Data)->webmailUrl;

        return Helper\redirectByUrl($link, []);
    }

}