<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Client;

use ModulesGarden\Servers\Qboxmail\Core\Helper;
use ModulesGarden\Servers\Qboxmail\Core\UI\Traits\WhmcsParams;
use ModulesGarden\Servers\Qboxmail\Core\Http\AbstractClientController;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Pages\EmailAccount as EmailAccountPage;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Pages\EmailAccountDescription;

class EmailAccount extends AbstractClientController
{
    use WhmcsParams;

    public function index()
    {
        return Helper\view()
            ->addElement(EmailAccountDescription::class)
            ->addElement(EmailAccountPage::class);
    }
}
