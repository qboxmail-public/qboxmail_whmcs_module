<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Client;

use ModulesGarden\Servers\Qboxmail\Core\Helper;
use ModulesGarden\Servers\Qboxmail\Core\UI\Traits\WhmcsParams;
use ModulesGarden\Servers\Qboxmail\Core\Http\AbstractClientController;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Pages\EmailAliasDescription;


class EmailAlias extends AbstractClientController
{
    use WhmcsParams;

    public function index()
    {
        return Helper\view()
            ->addElement(EmailAliasDescription::class)
            ->addElement(\ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Pages\EmailAlias::class);
    }
}
