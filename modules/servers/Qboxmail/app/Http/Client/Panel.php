<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Client;

use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\Core\Http\AbstractClientController;
use ModulesGarden\Servers\Qboxmail\Core\Helper;

class Panel extends AbstractClientController
{
    public function index()
    {
        $serviceId = $this->request->get('id');
        $api       = new QboxmailApi();
        $response = $api->postmasterSSO($serviceId);
       


        return Helper\redirectByUrl($response->url, []);
    }

}