<?php

namespace ModulesGarden\Servers\Qboxmail\App\Http\Client;

use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\App\Service\CustomFields\CustomFields;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Home\Pages\Dashboard;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Home\Pages\ValidateDomain;
use ModulesGarden\Servers\Qboxmail\Core\Http\AbstractClientController;
use ModulesGarden\Servers\Qboxmail\Core\Helper;
use ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\CustomField;
use ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\CustomFieldValue;
use ModulesGarden\Servers\Qboxmail\Core\UI\Traits\WhmcsParams;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 10.09.19
 * Time: 10:13
 * Class Home
 */
class Home extends AbstractClientController
{
    use WhmcsParams;

    public function index()
    {
        if ($this->getWhmcsParamByKey('status') === 'Active') {

            $view = Helper\view();

            $serviceId = $this->request->get('id');

            $isValid = CustomFieldRepository::getCustomFieldValueByServiceId($serviceId, CustomFieldRepository::QBOXMAIL_DOMAIN_CHECKED);

            if ($isValid) {
                $view->addElement(Dashboard::class);
            } else {
                $view->addElement(ValidateDomain::class);
            }

            return $view;
        }
    }

    public function verifyDomain()
    {
        try {
            $id  = $this->request->get('id');
            $api = new QboxmailApi();
            $api->validateDnsOwnership($id);

            header("Location: clientarea.php?action=productdetails&id=$id&modop=custom&mg-action=index&mg-page=Home&status=unverified");
            die;
        } catch (\Exception $e) {
            //VALID DOMAIN
            if ($e->getCode() == 404) {
                CustomFieldRepository::updateCustomFieldValueByServiceID($id, CustomFieldRepository::QBOXMAIL_DOMAIN_CHECKED, 'on');
            }

            header("Location: clientarea.php?action=productdetails&id=$id&modop=custom&mg-action=index&mg-page=Home&status=success");
            die;
        }


    }

}
