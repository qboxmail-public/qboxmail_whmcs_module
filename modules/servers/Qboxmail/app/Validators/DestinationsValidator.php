<?php

namespace ModulesGarden\Servers\Qboxmail\App\Validators;


use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Validators\BaseValidator;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 10.10.19
 * Time: 11:20
 * Class PasswordsValidator
 */
class DestinationsValidator extends BaseValidator
{


    /**
     * return true if data is valid, false if not,
     * add error messages to $errorsList
     *
     * @param $data           mixed
     * @param $additionalData mixed
     *
     * @return boolean
     */
    protected function validate($data, $additionalData = null)
    {
        $destinations = explode(',', $data);

        if (count($destinations) > 20) {
            $this->addValidationError('tooMuchDestinationsError');

            return false;
        }

        return true;
    }
}
