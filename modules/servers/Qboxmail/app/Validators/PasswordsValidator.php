<?php

namespace ModulesGarden\Servers\Qboxmail\App\Validators;


use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Validators\BaseValidator;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 10.10.19
 * Time: 11:20
 * Class PasswordsValidator
 */
class PasswordsValidator extends BaseValidator
{


    /**
     * return true if data is valid, false if not,
     * add error messages to $errorsList
     *
     * @param $data           mixed
     * @param $additionalData mixed
     *
     * @return boolean
     */
    protected function validate($data, $additionalData = null)
    {
        $formData = $additionalData->get('formData');

        if (isset($formData['editForm'])) {
            if (empty($data)) {
                return true;
            }
        }

        if (strlen($data) < 8 || ! preg_match('~[0-9]+~', $data)) {
            $this->addValidationError('passwordCharsLengthError');

            return false;
        }
        
        return true;
    }
}
