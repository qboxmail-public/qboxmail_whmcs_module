<?php

namespace ModulesGarden\Servers\Qboxmail\App\Validators;


use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Validators\BaseValidator;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Validators\boolen;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 10.10.19
 * Time: 11:21
 * Class RepeatPasswordValidator
 */
class RepeatPasswordValidator extends BaseValidator
{

    /**
     * return true if data is valid, false if not,
     * add error messages to $errorsList
     *
     * @param $data           mixed
     * @param $additionalData mixed
     *
     * @return boolean
     */
    protected function validate($data, $additionalData = null)
    {
        $formData = $additionalData->get('formData');
        if ($data !== $formData['password']) {
            $this->addValidationError('passwordsIsNotTheSame');

            return false;
        }

        return true;
    }
}
