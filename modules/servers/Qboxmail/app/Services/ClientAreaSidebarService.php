<?php
namespace ModulesGarden\Servers\Qboxmail\App\Services;

;

use ModulesGarden\Servers\Qboxmail\App\Traits\HostingService;
use ModulesGarden\Servers\Qboxmail\App\Traits\ProductService;
use function ModulesGarden\Servers\Qboxmail\Core\Helper\sl;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Sidebar\Sidebar;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Sidebar\SidebarService;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 10.09.19
 * Time: 10:23
 * Class ClientAreaSidebarService
 */
class ClientAreaSidebarService
{

    const SUPPORTED_SERVER_TYPES = ['Qboxmail'];

    use HostingService;
    use \ModulesGarden\Servers\Qboxmail\Core\UI\Traits\WhmcsParams;
    use ProductService;

    /**
     * @var \WHMCS\View\Menu\Item
     */
    private $primarySidebar;
    protected $hosting;

    /**
     * ClientAreaSidebarService constructor.
     *
     * @param $hostingId
     */
    public function __construct($hostingId, \WHMCS\View\Menu\Item $primarySidebar)
    {
        $this->setHostingId($hostingId);
        $this->setHosting();
        $this->primarySidebar = $primarySidebar;
    }

    private function setHosting()
    {
        $this->hosting = \WHMCS\Service\Service::find($this->hostingId);

        return $this;
    }

    /**
     * @return $this|void
     */
    public function informationReplaceUri()
    {
        $overview = $this->primarySidebar->getChild('Service Details Overview');
        if ( ! is_a($overview, '\WHMCS\View\Menu\Item')) {
            return;
        }
        $panel = $overview->getChild('Information');
        if ( ! is_a($panel, '\WHMCS\View\Menu\Item')) {
            return;
        }
        $panel->setUri("clientarea.php?action=productdetails&id={$this->getHostingId()}");
        $panel->setAttributes([]);

        return $this;
    }

    /**
     * @return $this|void
     */
    public function changePasswordReplaceUri()
    {
        $actions = $this->primarySidebar->getChild('Service Details Overview');
        if ( ! is_a($actions, '\WHMCS\View\Menu\Item')) {
            return;
        }
        $panel = $actions->getChild('Change Password');
        if ( ! is_a($panel, '\WHMCS\View\Menu\Item')) {
            return;
        }
        $panel->setUri("clientarea.php?action=productdetails&id={$this->getHostingId()}#tabChangepw");
        $panel->setAttributes([]);

        return $this;
    }

    /**
     *
     */
    public function build()
    {
        if ( ! in_array($this->hosting()->product->servertype, self::SUPPORTED_SERVER_TYPES)) {
            return;
        }

        /**
         * @var SidebarService $sidebarService
         */
        $sidebarService = sl("sidebar");
        $this->setProductId($this->hosting()->packageid);


        $lang  = sl("lang");
        $order = 671;
        foreach ($sidebarService->get() as $sidebar) {
            /**
             * @var Sidebar $sidebar
             */
            $newPanel = [
                'label' => $lang->abtr($sidebar->getTitle()),
                'order' => $order,
            ];
            $order++;
            $childPanel = $this->primarySidebar->addChild($sidebar->getName(), $newPanel);
            foreach ($sidebar->getChildren() as $sidebarItem) {

                /**
                 *
                 * add item to menu
                 */
                $newItem = [
                    'label'   => $lang->abtr($sidebarItem->getTitle()),
                    'uri'     => str_replace('{$hostingId}', $this->getHostingId(), $sidebarItem->getHref()),
                    'order'   => $sidebarItem->getOrder(),
                    "current" => $_REQUEST['mg-page'] == $sidebarItem->getName() ? true : false,
                ];

                $childPanel->addChild($sidebarItem->getName(), $newItem);
            }
        }
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        if ($this->hosting->domainstatus != 'Active') {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isSupportedModule()
    {
        if ($this->hosting->product->servertype != 'Qboxmail') {
            return false;
        }

        return true;
    }
}
