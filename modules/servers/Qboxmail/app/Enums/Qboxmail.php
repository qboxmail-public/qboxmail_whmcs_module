<?php
namespace ModulesGarden\Servers\Qboxmail\App\Enums;


class Qboxmail
{
    //for api
    const MAX_EMAIL_ACCOUNTS = 'max_email_accounts';
    const MAX_EMAIL_ACCOUNTS_25GB = 'max_email_accounts_25gb';
    const MAX_EMAIL_ACCOUNTS_50GB = 'max_email_accounts_50gb';
    const MAX_EMAIL_ACCOUNTS_100GB = 'max_email_accounts_100gb';

    //for configurable options
    const EMAIL_ACCOUNTS_8GB = 'email_accounts_8gb';
    const EMAIL_ACCOUNTS_25GB = 'email_accounts_25gb';
    const EMAIL_ACCOUNTS_50GB = 'email_accounts_50gb';
    const EMAIL_ACCOUNTS_100GB = 'email_accounts_100gb';

    const MAX_EMAIL_QUOTA = 'max_email_quota';
    const PLAN = 'plan';

    const BASIC = 'basic';
    const PROFESSIONAL = 'professional';
    const ENTERPRISE = 'enterprise';


    const OPTIONS = [
        self::EMAIL_ACCOUNTS_8GB,
        self::EMAIL_ACCOUNTS_25GB,
        self::EMAIL_ACCOUNTS_50GB,
        self::EMAIL_ACCOUNTS_100GB,
    ];

    /**
     * labels for statuses
     */
    const STATUS_LABEL = [
        'enabled'  => 'success',
        'disabled' => 'warning',
        'failure'  => 'danger',
        'updating' => 'info',
        'default'  => 'default',
    ];

}