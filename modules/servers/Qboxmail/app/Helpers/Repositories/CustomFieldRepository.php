<?php

namespace ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories;

use ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\CustomField;
use ModulesGarden\Servers\Qboxmail\App\Models\CustomFieldValueModel;
use \WHMCS\Database\Capsule as DB;

class CustomFieldRepository
{
    const QBOXMAIL_DOMAIN_ID = 'domain_id';
    const QBOXMAIL_PASSWORD = 'password';
    const QBOXMAIL_DOMAIN_CHECKED = 'domain_checked';

    public static function getCustomField($name, $relid)
    {
        return CustomField::where('fieldname', 'like', $name . '|%')->where('relid', $relid)->first();
    }

    public static function saveCustomFieldValue($relid, $packageId, $value, $fieldName)
    {
        $fieldId = self::getContextCustomField($packageId, $fieldName);

        return CustomFieldValueModel::updateOrCreate(
            ['fieldid' => $fieldId, 'relid' => $relid], ['value' => $value]
        );
    }

    public static function getCustomFieldValue($packageId, $fieldName)
    {
        $fieldId = self::getContextCustomField($packageId, $fieldName);

        return CustomFieldValueModel::where('fieldid', $fieldId)->value('value');
    }

    public static function getCustomFieldValueByServiceId($serviceId, $fieldName)
    {
        return DB::table('tblcustomfieldsvalues')
            ->join('tblcustomfields', 'tblcustomfieldsvalues.fieldid', 'tblcustomfields.id')
            ->where('tblcustomfieldsvalues.relid', $serviceId)
            ->where('tblcustomfields.fieldname', 'LIKE', "$fieldName|%")
            ->value('tblcustomfieldsvalues.value');

    }

    public static function updateCustomFieldValueByServiceID($serviceId, $fieldName, $value)
    {
        $id = DB::table('tblcustomfieldsvalues')
            ->join('tblcustomfields', 'tblcustomfieldsvalues.fieldid', 'tblcustomfields.id')
            ->where('tblcustomfieldsvalues.relid', $serviceId)
            ->where('tblcustomfields.fieldname', 'LIKE', "$fieldName|%")
            ->value('tblcustomfieldsvalues.id');

        DB::table('tblcustomfieldsvalues')->where('id', $id)->update(['value' => $value]);

    }


    public static function getContextCustomField($packageId, $fieldName)
    {
        return CustomFieldRepository::getCustomField($fieldName, $packageId)->id;
    }


    public static function createPasswordField($productId)
    {
        $field = CustomField::where([
            ['fieldname', 'LIKE', self::QBOXMAIL_PASSWORD . '%'],
            ['relid', $productId],
        ])->first();

        if ($field) {
            return false;
        }
        $customField              = new CustomField();
        $customField->type        = 'product';
        $customField->relid       = $productId;
        $customField->fieldname   = self::QBOXMAIL_PASSWORD . '| Postmaster password';
        $customField->fieldtype   = 'password';
        $customField->required    = 'on';
        $customField->showorder   = 'on';
        $customField->description = 'Password for the user Postmaster (min 8 chars and at least one number).';
        $customField->regexpr     = "/^(?=.*[a-zA-Z])(?=.*\d).{8,}$/";
        $customField->save();

        return true;
    }

    public static function createDomainChecked($productId)
    {
        $field = CustomField::where([
            ['fieldname', 'LIKE', self::QBOXMAIL_DOMAIN_CHECKED . '%'],
            ['relid', $productId],
        ])->first();

        if ($field) {
            return false;
        }
        $customField            = new CustomField();
        $customField->type      = 'product';
        $customField->relid     = $productId;
        $customField->fieldname = self::QBOXMAIL_DOMAIN_CHECKED . '| Domain Checked';
        $customField->fieldtype = 'tickbox';
        $customField->adminonly = 'on';
        $customField->save();

        return true;
    }

    public static function createDomainId($productId)
    {
        $field = CustomField::where([
            ['fieldname', 'LIKE', self::QBOXMAIL_DOMAIN_ID . '%'],
            ['relid', $productId],
        ])->first();

        if ($field) {
            return false;
        }
        $customField            = new CustomField();
        $customField->type      = 'product';
        $customField->relid     = $productId;
        $customField->fieldname = self::QBOXMAIL_DOMAIN_ID . '| Domain ID';
        $customField->fieldtype = 'text';
        $customField->adminonly = 'on';
        $customField->save();

        return true;
    }

}
