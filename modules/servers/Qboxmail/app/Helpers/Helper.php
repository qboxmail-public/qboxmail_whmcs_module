<?php

namespace ModulesGarden\Servers\Qboxmail\App\Helpers;

use ModulesGarden\Servers\Qboxmail\App\Models\ProductConfiguration;

class Helper
{
    public static function moduleParams($hid)
    {
        if ( ! function_exists("ModuleBuildParams")) {
            require_once ROOTDIR . '/includes/modulefunctions.php';
        }

        return ModuleBuildParams($hid);
    }

    public static function accountsLimit($params)
    {
        $config = ProductConfiguration::config($params['packageid']);
        $limit  = $config['email_accounts'] > $params['configoptions']['acc_limit'] ? $config['email_accounts'] : $params['configoptions']['acc_limit'];

        return $limit;
    }

    public static function aliasesLimit($params)
    {
        $config = ProductConfiguration::config($params['packageid']);
        $limit  = $config['alias_limit'] > $params['configoptions']['alias_limit'] ? $config['alias_limit'] : $params['configoptions']['alias_limit'];

        return $limit;
    }

    public static function forwardingAliasesLimit($params)
    {
        $config = ProductConfiguration::config($params['packageid']);
        $limit  = $config['forwarding_alias_limit'] > $params['configoptions']['forwarding_alias_limit'] ? $config['forwarding_alias_limit'] : $params['configoptions']['forwarding_alias_limit'];

        return $limit;
    }

    public static function quotaLimit($params)
    {
        $config = ProductConfiguration::config($params['packageid']);
        $limit  = $config['user_quota'] > $params['configoptions']['user_quota'] ? $config['user_quota'] : $params['configoptions']['user_quota'];

        return $limit;
    }
}
