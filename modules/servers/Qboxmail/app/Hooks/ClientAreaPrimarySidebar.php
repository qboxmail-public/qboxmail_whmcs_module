<?php

use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;

$hookManager->register(
    function (\WHMCS\View\Menu\Item $primarySidebar) {
        $request = \ModulesGarden\Servers\Qboxmail\Core\Helper\sl('request');


        if ( ! $request->getSession('uid')) {
            return;
        }

        $serviceId = $request->get('id');

        if ( ! $serviceId) {
            return;
        }

        $isValid = CustomFieldRepository::getCustomFieldValueByServiceId($serviceId, CustomFieldRepository::QBOXMAIL_DOMAIN_CHECKED);
        if ($isValid != 'on') {
            return;
        }

        $clientAreaSideBar = new \ModulesGarden\Servers\Qboxmail\App\Services\ClientAreaSidebarService($request->get("id"),
            $primarySidebar);
        if ( ! $clientAreaSideBar->isActive() || ! $clientAreaSideBar->isSupportedModule()) {
            return;
        }
        if ( ! function_exists('ModuleBuildParams')) {
            require_once \ModulesGarden\Servers\Qboxmail\Core\ModuleConstants::getFullPathWhmcs('includes') . DIRECTORY_SEPARATOR . "modulefunctions.php";
        }
        $params = \ModuleBuildParams($request->get("id"));
        \ModulesGarden\Servers\Qboxmail\Core\Helper\sl("whmcsParams")->setParams($params);


        if ($request->get('action') == "cancel") {
            $clientAreaSideBar->informationReplaceUri();
        } else {
            if ($request->get('action') == "productdetails") {
                $clientAreaSideBar->informationReplaceUri();
                $clientAreaSideBar->build();
            }
        }
    }, 943
);
