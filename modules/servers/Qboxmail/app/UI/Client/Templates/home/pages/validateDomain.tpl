<div style="display: none; margin: 20px auto;"
     class="lu-alert lu-alert--danger lu-alert--outline lu-alert--float lu-alert--border-left lu-has-icon verify-alert">
    {$MGLANG->absoluteT('addonCA','verifyDomain','unverified')}
</div>
<div class="alert lu-alert--info">
    <h4 class="lu-text-white">{$MGLANG->absoluteT('addonCA','verifyDomain','title')}</h4>
    <p>{$MGLANG->absoluteT('addonCA','verifyDomain','instruction')}</p>
    <div>
        <table class="lu-table">
            <thead>
            <tr>
                <th>{$MGLANG->absoluteT('addonCA','verifyDomain','record_name')}</th>
                <th>{$MGLANG->absoluteT('addonCA','verifyDomain','record_type')}</th>
                <th>{$MGLANG->absoluteT('addonCA','verifyDomain','record_ip')}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>{$rawObject->getDomain()}</th>
                <th>A</th>
                <th>{$rawObject->getIp()}</th>
            </tr>
            </tbody>
        </table>
    </div>
    <div>
        <p>{$MGLANG->absoluteT('addonCA','verifyDomain','additional_info')}</p>
    </div>
    <a class="lu-btn"
       href="clientarea.php?action=productdetails&id={$rawObject->getHostingId()}&modop=custom&mg-action=verifyDomain&mg-page=Home">{$MGLANG->absoluteT('addonCA','verifyDomain','check_dns_button')}</a>
</div>


<script>
    $(document).ready(function () {
        let url = new URL(window.location.href);
        let searchParams = new URLSearchParams(url.search);

        if (searchParams.get('status') === 'unverified') {
            $('.verify-alert').show();
        }
    });

</script>

