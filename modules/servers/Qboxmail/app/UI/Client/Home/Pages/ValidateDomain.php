<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\Home\Pages;

use ModulesGarden\Servers\Qboxmail\App\Helpers\Helper;
use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\App\Service\ConfigurableOptions\ConfigurableOptions;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Home\Buttons\VerifyDomainButton;
use ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\Hosting;
use ModulesGarden\Servers\Qboxmail\Core\UI\Builder\BaseContainer;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Modals\ModalConfirmSuccess;


class ValidateDomain extends BaseContainer implements ClientArea
{
    protected $id = 'validateDomain';
    protected $name = 'validateDomain';
    protected $title = null;

    /**
     * load buttons
     */
    public function initContent()
    {
        $this->initButton();
    }

    public function initButton()
    {
        $this->addElement(new ModalConfirmSuccess('success'));
        $verifyDomainButton = new VerifyDomainButton();
        $this->addButton($verifyDomainButton);
    }

    public function getHostingId()
    {
        return $this->request->get('id');
    }

    public function getIp()
    {
        return (new \ModulesGarden\Servers\Qboxmail\Core\Configuration\Data)->ipAddress;
    }

    public function getDomain()
    {
        $id      = $this->request->get('id');
        $service = Hosting::where('id', $id)->first();

        $code = CustomFieldRepository::getCustomFieldValueByServiceId($id, CustomFieldRepository::QBOXMAIL_DOMAIN_ID);

        return "$code.$service->domain";
    }

}

