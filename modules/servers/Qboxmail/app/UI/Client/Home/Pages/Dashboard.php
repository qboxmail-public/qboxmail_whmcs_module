<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\Home\Pages;

use ModulesGarden\Servers\Qboxmail\App\Helpers\BuildUrlExtended;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Home\Fields\FeatureField;
use ModulesGarden\Servers\Qboxmail\Core\UI\Builder\BaseContainer;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 10.09.19
 * Time: 10:09
 * Class Dashboard
 */
class Dashboard extends BaseContainer implements ClientArea
{

    /**
     * @var array
     */
    protected $featureContainer = [];

    public function initContent()
    {
        $this->initFeatures();
    }

    /**
     *
     */
    protected function initFeatures()
    {
        $feature = new FeatureField('emailAccount');
        $feature->setUrl(BuildUrlExtended::getProvisioningUrl('emailAccount'));
        $this->addFeature($feature);

        $feature = new FeatureField('emailAlias');
        $feature->setUrl(BuildUrlExtended::getProvisioningUrl('emailAlias'));
        $this->addFeature($feature);

        $feature = new FeatureField('goToPanel');
        $feature->setUrl(BuildUrlExtended::getProvisioningUrl('panel'));
        $feature->setTargetBlank(true);
        $this->addFeature($feature);

        $feature = new FeatureField('goToWebmail');
        $feature->setUrl(BuildUrlExtended::getProvisioningUrl('webmail'));
        $feature->setTargetBlank(true);
        $this->addFeature($feature);
    }

    /**
     * @param  FeatureField  $feature
     *
     * @return $this
     */
    protected function addFeature(FeatureField $feature)
    {
        $this->featureContainer[$feature->getId()] = $feature;

        return $this;
    }

    /**
     * @return array
     */
    public function getFeatures()
    {
        return $this->featureContainer;
    }

}
