<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\Home\Fields;

use ModulesGarden\Servers\Qboxmail\Core\Helper\BuildUrl;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\BaseField;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 03.10.19
 * Time: 14:37
 * Class FeatureField
 */
class FeatureField extends BaseField implements ClientArea
{
    protected $id = 'featureField';
    protected $name = 'featureField';
    protected $url;

    protected $targetBlank = false;

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param $url
     *
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return bool|string|null
     */
    public function getIcon()
    {
        $asset = BuildUrl::getAppAssetsURL();

        return $asset . DIRECTORY_SEPARATOR . 'icons' . DIRECTORY_SEPARATOR . $this->id . '.png';
    }

    /**
     * @return bool
     */
    public function isTargetBlank()
    {
        return $this->targetBlank;
    }

    /**
     * @param $targetBlank
     *
     * @return $this
     */
    public function setTargetBlank($targetBlank)
    {
        $this->targetBlank = $targetBlank;

        return $this;
    }


}
