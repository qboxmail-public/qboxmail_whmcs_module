<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\Home\Buttons;

use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Buttons\ButtonBase;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 11:50
 * Class AddEmailAliasButton
 */
class VerifyDomainButton extends ButtonBase implements ClientArea
{
    protected $htmlAttributes = [];

    protected $id = 'verifyDomain';
    protected $title = 'verifyDomain';
}