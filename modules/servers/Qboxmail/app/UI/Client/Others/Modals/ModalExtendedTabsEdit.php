<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Modals;

use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\AdminArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Modals\ModalTabsEdit;

class ModalExtendedTabsEdit extends ModalTabsEdit implements AdminArea
{
    protected $id = 'modalExtendedTabsEdit';
    protected $name = 'modalExtendedTabsEdit';
    protected $title = 'modalExtendedTabsEdit';

    protected function afterInitContent()
    {
        parent::afterInitContent();

        foreach ($this->forms as &$form) {
            $form->getHtml();
        }
    }
}
