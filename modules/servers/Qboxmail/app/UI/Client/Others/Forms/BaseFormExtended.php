<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Forms;


use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\BaseForm;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 12.11.19
 * Time: 14:57
 * Class BaseFormExtended
 */
class BaseFormExtended extends BaseForm
{
    protected $id = 'baseFormExtended';
    protected $name = 'baseFormExtended';
}