<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Pages;

use ModulesGarden\Servers\Qboxmail\Core\Helper\BuildUrl;
use ModulesGarden\Servers\Qboxmail\Core\UI\Builder\BaseContainer;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Traits\Alerts;


class Description extends BaseContainer implements ClientArea
{
    use Alerts;

    protected $id = 'description';
    protected $name = 'description';
    protected $title = 'description';
    protected $description;

    public function __construct($baseId = null, $hasAlert = false, $type = null)
    {
        parent::__construct($baseId);
        $this->setTitle($baseId . 'PageTitle');
        $this->setDescription($baseId . 'PageDescription');
        if ($hasAlert) {
            $this->addInternalAlert($baseId . 'AlertDescription', $type);
        }
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getAssetsUrl()
    {
        return BuildUrl::getAssetsURL();
    }

    public function getIcon()
    {
        $asset = BuildUrl::getAppAssetsURL();

        return $asset . DIRECTORY_SEPARATOR . 'icons' . DIRECTORY_SEPARATOR . $this->id . '.png';
    }
}
