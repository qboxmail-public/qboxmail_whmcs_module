<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Sections;

use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\AdminArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\BaseSection;

/**
 * Class FreeFieldsSection
 * User: Nessandro
 * Date: 2019-09-20
 * Time: 12:44
 */
class FreeFieldsSection extends BaseSection implements ClientArea, AdminArea
{
    protected $id = 'freeFieldsSection';
    protected $name = 'freeFieldsSection';
}
