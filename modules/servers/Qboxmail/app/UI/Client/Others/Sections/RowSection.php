<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Sections;

use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\SectionLuRow;

class RowSection extends SectionLuRow
{
    protected $id = 'rowSection';
    protected $name = 'rowSection';
}
