<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Fields;

use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\BaseField;

/**
 * Select field controler
 *
 * @author Sławomir Miśkowicz <slawomir@modulesgarden.com>
 */
class OptGroupSelect extends BaseField
{
    protected $id = 'optGroupSelect';
    protected $name = 'optGroupSelect';
    protected $multiple = false;
    protected $optGroups = [];

    protected $availableValues = [];

    protected $htmlAttributes = [
        '@change' => 'selectChangeAction($event)',
    ];

    public function setOptGroups($values)
    {
        $this->optGroups = $values;
    }

    public function getGroupValues($group)
    {
        return $this->optGroups[$group];
    }

    public function getOptGroups()
    {
        return $this->optGroups;
    }

    public function setSelectedValue($value)
    {
        $this->value = $value;

        return $this;
    }

    public function setAvailableValues($values)
    {
        if (is_array($values)) {
            $this->availableValues = $values;
        }

        return $this;
    }

    public function getAvailableValues()
    {
        return $this->availableValues;
    }

    public function isMultiple()
    {
        return $this->multiple;
    }

    public function enableMultiple()
    {
        $this->multiple = true;

        return $this;
    }

    public function disableMultiple()
    {
        $this->multiple = false;

        return $this;
    }
}
