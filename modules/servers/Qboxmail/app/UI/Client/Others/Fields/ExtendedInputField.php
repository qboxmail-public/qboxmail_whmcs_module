<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Fields;


use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\AdminArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Text;

class ExtendedInputField extends Text implements AdminArea, ClientArea
{
    protected $id = 'extendedInputField';
    protected $name = 'extendedInputField';
    protected $fieldType = self::TYPE_DEFAULT;

    const TYPE_PASSWORD = 'password';
    const TYPE_EMAIL = 'email';
    const TYPE_TEXT = 'text';
    const TYPE_NUMBER = 'number';
    const TYPE_DEFAULT = 'text';

    protected $rawDescription = null;

    /**
     * @return string
     */
    public function getFieldType()
    {
        return $this->fieldType;
    }

    /**
     * @param $fieldType
     *
     * @return $this
     */
    public function setFieldType($fieldType)
    {
        $this->fieldType = $fieldType;

        return $this;
    }

    /**
     * @return null
     */
    public function getRawDescription()
    {
        return $this->rawDescription;
    }

    /**
     * @param $rawDescription
     *
     * @return $this
     */
    public function setRawDescription($rawDescription)
    {
        $this->rawDescription = $rawDescription;

        return $this;
    }

    public function isRawDescription()
    {
        return $this->rawDescription !== null;
    }


}