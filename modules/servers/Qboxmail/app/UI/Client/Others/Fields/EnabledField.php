<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Fields;


use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\BaseField;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 12.11.19
 * Time: 10:49
 * Class EnabledField
 */
class EnabledField extends BaseField
{
    protected $id = 'enabledField';
    protected $name = 'enabledField';
    protected $title = 'field_enabled_';

    const TYPE_SUCCESS = 'success';
    const TYPE_DANGER = 'danger';

    protected $enabled = false;
    protected $rawType = false;

    /**
     * @return bool
     */
    public function isEnabled()
    {

        return $this->enabled;
    }

    /**
     * @param  bool  $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return string
     */
    public function getType()
    {
        if ($this->getRawType()) {
            return $this->getRawType();
        }

        if ($this->isEnabled()) {
            $type = self::TYPE_SUCCESS;
        } else {
            $type = self::TYPE_DANGER;
        }

        return $type;
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        /**
         * if raw return raw
         */
        if ($this->titleRaw) {
            return $this->titleRaw;
        }

        /**
         * return type depending on status
         */
        if ($this->isEnabled()) {
            $title = $this->title . self::TYPE_SUCCESS;
        } else {
            $title = $this->title . self::TYPE_DANGER;
        }

        /**
         *
         */
        return $title;
    }

    /**
     * @return bool
     */
    public function getRawType()
    {
        return $this->rawType;
    }

    /**
     * @param  bool  $rawType
     */
    public function setRawType($rawType)
    {
        $this->rawType = $rawType;
    }


}