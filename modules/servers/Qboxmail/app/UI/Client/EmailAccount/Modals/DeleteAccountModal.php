<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Modals;


use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Forms\DeleteAccountForm;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Forms\EditAccountForm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Modals\BaseModal;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 11:28
 * Class DeleteAccountModal
 */
class DeleteAccountModal extends BaseModal implements ClientArea
{

    protected $id = 'deleteAccountModal';
    protected $name = 'deleteAccountModal';
    protected $title = 'deleteAccountModal';

    public function initContent()
    {
        $this->setSubmitButtonClassesDanger();
        $this->setModalTitleTypeDanger();

        $this->addForm(new DeleteAccountForm());
    }

}