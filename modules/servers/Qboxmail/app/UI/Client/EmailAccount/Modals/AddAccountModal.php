<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Modals;

use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Modals\ModalExtendedTabsEdit;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Forms\AddAccountForm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Modals\BaseEditModal;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 10.09.19
 * Time: 13:06
 * Class AddAccountModal
 */
class AddAccountModal extends ModalExtendedTabsEdit implements ClientArea
{
    protected $id = 'addAccountModal';
    protected $name = 'addAccountModal';
    protected $title = 'addAccountModal';

    public function initContent()
    {
        $this->addForm(new AddAccountForm());
    }
}
