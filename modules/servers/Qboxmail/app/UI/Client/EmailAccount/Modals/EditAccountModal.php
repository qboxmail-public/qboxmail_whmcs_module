<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Modals;


use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Modals\ModalExtendedTabsEdit;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Forms\AddAccountForm;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Forms\EditAccountForm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Modals\BaseEditModal;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 09:30
 * Class EditAccountModal
 */
class EditAccountModal extends ModalExtendedTabsEdit implements ClientArea
{
    protected $id = 'editAccountModal';
    protected $name = 'editAccountModal';
    protected $title = 'editAccountModal';

    public function initContent()
    {
        $this->addForm(new EditAccountForm());
    }
}