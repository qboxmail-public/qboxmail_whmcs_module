<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Providers;

use Exception;
use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\Core\UI\ResponseTemplates\HtmlDataJsonResponse;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\DataProviders\BaseDataProvider;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 11:28
 * Class DeleteAccountDataProvider
 */
class DeleteAccountDataProvider extends BaseDataProvider
{

    public function read()
    {
        $this->data['id'] = $this->actionElementId;
    }

    public function update()
    {
    }

    public function delete()
    {
        try {
            $serviceId = $this->request->get('id');
            $api       = new QboxmailApi();
            
            $api->deleteEmail($serviceId, $this->formData['id']);

            return (new HtmlDataJsonResponse())->setMessageAndTranslate('emailAccountHasBeenDeleted')->setStatusSuccess();
        } catch (Exception $e) {
            return (new HtmlDataJsonResponse())->setMessageAndTranslate('emailAccountsDeleteError')->setStatusError();
        }
    }
}