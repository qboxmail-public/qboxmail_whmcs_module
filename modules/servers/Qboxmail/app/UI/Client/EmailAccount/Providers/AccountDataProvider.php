<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Providers;

use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\App\Libs\Product\ProductHelper;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Providers\ProductConfigurationDataProvider;
use ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\Hosting;
use ModulesGarden\Servers\Qboxmail\Core\UI\ResponseTemplates\HtmlDataJsonResponse;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\DataProviders\BaseDataProvider;
use function ModulesGarden\Servers\Qboxmail\Core\Helper\di;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 10.09.19
 * Time: 13:06
 * Class AccountDataProvider
 */
class AccountDataProvider extends BaseDataProvider
{
    public function read()
    {
        $hid = $this->request->get('id');

        $hosting       = Hosting::where('id', $hid)->first();
        $api           = new QboxmailApi();
        $data          = $api->getDomain($hid);
        $maxEmailQuota = ProductHelper::bytesToGib($data->resources[0]->max_email_quota);

        $this->data['domain']                   = $hosting->domain;
        $this->data['max_email_accounts']       = $hosting->max_email_accounts;
        $this->data['max_email_accounts_25gb']  = $hosting->max_email_accounts_25gb;
        $this->data['max_email_accounts_50gb']  = $hosting->max_email_accounts_50gb;
        $this->data['max_email_accounts_100gb'] = $hosting->max_email_accounts_100gb;

        foreach (ProductConfigurationDataProvider::QBOXMAIL_QUOTA as $name => $value) {
            if ($value > $maxEmailQuota) {
                continue;
            }
            $this->availableValues['max_email_size'][$value] = $name;
        }

    }

    public function create()
    {

        try {
            $maxEmailSizeGib = $this->formData['max_email_size'];
            $serviceId       = $this->request->get('id');
            $parameters      = [
                'firstname'             => $this->formData['firstname'],
                'lastname'              => $this->formData['lastname'],
                'password'              => $this->formData['password'],
                'password_confirmation' => $this->formData['repeat_password'],
                'name'                  => $this->formData['username'],
                'max_email_quota'       => ProductHelper::GibToBytes($maxEmailSizeGib),
            ];

            $api = new QboxmailApi();
            $api->createEmail($serviceId, $parameters);

            return (new HtmlDataJsonResponse())->setMessageAndTranslate('emailAccountHasBeenCreated')->setStatusSuccess();
        } catch (\Exception $e) {
            $message = di('lang')->absoluteT('APIError', $e->getMessage());

            return (new HtmlDataJsonResponse())->setMessage($message)->setStatusError();

        }


    }

    public function update()
    {

    }

    public function delete()
    {

    }
}
