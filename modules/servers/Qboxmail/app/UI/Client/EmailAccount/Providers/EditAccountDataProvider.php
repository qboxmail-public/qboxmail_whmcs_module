<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Providers;


use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\App\Libs\Product\ProductHelper;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Providers\ProductConfigurationDataProvider;
use ModulesGarden\Servers\Qboxmail\Core\UI\ResponseTemplates\HtmlDataJsonResponse;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\DataProviders\BaseDataProvider;
use function ModulesGarden\Servers\Qboxmail\Core\Helper\di;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 09:35
 * Class EditAccountDataProvider
 */
class EditAccountDataProvider extends BaseDataProvider
{
    public function read()
    {
        $serviceId = $this->request->get('id');
        $emailCode = $this->request->get('actionElementId');

        $api   = new QboxmailApi();
        $res   = $api->getEmail($serviceId, $emailCode);
        $email = $res->resources[0];

        $res           = $api->getDomain($serviceId);
        $maxEmailQuota = ProductHelper::bytesToGib($res->resources[0]->max_email_quota);

        $this->data = [
            'id'             => $email->code,
            'firstname'      => $email->firstname,
            'lastname'       => $email->lastname,
            'status'         => $email->status === 'enabled' ? 'on' : '',
            'prevStatus'     => $email->status === 'enabled' ? 'on' : '',
            'max_email_size' => ProductHelper::bytesToGib($email->max_email_quota),
        ];

        foreach (ProductConfigurationDataProvider::QBOXMAIL_QUOTA as $name => $value) {
            if ($value > $maxEmailQuota) {
                continue;
            }
            $this->availableValues['max_email_size'][$value] = $name;
        }
    }

    public function update()
    {
        try {
            $serviceId       = $this->request->get('id');
            $emailCode       = $this->formData['id'];
            $maxEmailSizeGib = $this->formData['max_email_size'];
            $api             = new QboxmailApi();

            $email = $api->getEmail($serviceId, $emailCode)->resources[0];

            $editData = [];

            if ( ! empty($this->formData['password'])) {
                $editData['password']              = $this->formData['password'];
                $editData['password_confirmation'] = $this->formData['repeat_password'];
            }

            if ($email->firstname !== $this->formData['firstname']) {
                $editData['firstname'] = $this->formData['firstname'];
            }

            if ($email->lastname !== $this->formData['lastname']) {
                $editData['lastname'] = $this->formData['lastname'];
            }

            if (ProductHelper::bytesToGib($email->max_email_quota) != $maxEmailSizeGib) {
                $editData['max_email_quota'] = ProductHelper::GibToBytes($maxEmailSizeGib);
            }

            if (count($editData) > 0) {
                $api->editEmail($serviceId, $emailCode, $editData);
            }

            if ($this->formData['status'] !== $this->formData['prevStatus']) {
                $status = 'disabled';
                if ($this->formData['status'] == 'on') {
                    $status = 'enabled';
                }
                $api->editEmailStatus($serviceId, $emailCode, $status);
            }

            return (new HtmlDataJsonResponse())->setMessageAndTranslate('emailAccountHasBeenUpdated')->setStatusSuccess();
        } catch (\Exception $e) {
            $message = di('lang')->absoluteT('APIError', $e->getMessage());

            return (new HtmlDataJsonResponse())->setMessage($message)->setStatusError();

        }
    }

}