<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Sections;

use ModulesGarden\Servers\Qboxmail\App\Traits\FormExtendedTrait;
use ModulesGarden\Servers\Qboxmail\App\Libs\Product\ProductHelper;
use ModulesGarden\Servers\Qboxmail\App\Validators\PasswordsValidator;
use ModulesGarden\Servers\Qboxmail\App\Validators\RepeatPasswordValidator;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Hidden;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Password;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Text;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Select;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Switcher;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\InputGroup;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Fields\OptGroupSelect;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Fields\ExtendedInputField;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\InputGroupElements;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Sections\FreeFieldsSection;
use ModulesGarden\Servers\Qboxmail\App\Libs\Zimbra\Components\Api\Soap\Repository\ClassOfServices;
use function ModulesGarden\Servers\Qboxmail\Core\Helper\di;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 12.11.19
 * Time: 13:52
 * Class EditGeneralSection
 */
class EditGeneralSection extends FreeFieldsSection
{
    protected $id = 'editGeneralSection';
    protected $name = 'editGeneralSection';

    use FormExtendedTrait;

    public function initContent()
    {
        $field = new Hidden('editForm');
        $this->addField($field);
        $field = new Hidden('prevStatus');
        $this->addField($field);
        $field = new Hidden('prevFirstname');
        $this->addField($field);
        $field = new Hidden('prevLastname');
        $this->addField($field);
        $field = new Hidden('id');
        $this->addField($field);


        $this->generateDoubleSection([(new Text('firstname'))->notEmpty(), (new Text('lastname'))]);
        $passwd = new Password('password');
        $passwd->addValidator(new PasswordsValidator());
        $lang        = di('lang');
        $description = $lang->T('passDescription');
        $passwd->setDescription($description);

        $repPasswd = (new Password('repeat_password'))->addValidator(new RepeatPasswordValidator());
        $this->generateDoubleSection([$passwd, $repPasswd]);
        $field = new Switcher('status');
        $this->addField($field);

        $select = new Select('max_email_size');


        $this->generateDoubleSection([$select]);

    }
}