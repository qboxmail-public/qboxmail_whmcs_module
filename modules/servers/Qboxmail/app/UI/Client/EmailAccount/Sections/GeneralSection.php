<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Sections;

use ModulesGarden\Servers\Qboxmail\App\Traits\FormExtendedTrait;
use ModulesGarden\Servers\Qboxmail\App\Validators\EmailAmountValidator;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Hidden;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Text;
use ModulesGarden\Servers\Qboxmail\App\Validators\PasswordsValidator;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Select;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Password;
use ModulesGarden\Servers\Qboxmail\App\Validators\RepeatPasswordValidator;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\InputGroup;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\InputGroupElements;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Sections\FreeFieldsSection;
use function \ModulesGarden\Servers\Qboxmail\Core\Helper\di;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 12.11.19
 * Time: 13:39
 * Class GeneralSection
 */
class GeneralSection extends FreeFieldsSection
{
    protected $id = 'generalSection';
    protected $name = 'generalSection';

    use FormExtendedTrait;

    public function initContent()
    {
        $this->generateDoubleSection([(new Text('firstname'))->notEmpty(), (new Text('lastname'))]);
        $email = new InputGroup('usernameGroup');

        $email->addTextField('username', false, true);
        $email->addInputAddon('emailSign', false, '@');
        $email->addInputComponent((new InputGroupElements\Text('domain'))->notEmpty()->addHtmlAttribute('readonly',
            'true'));
        $this->addSection($email);

        $passwd      = new Password('password');
        $lang        = di('lang');
        $description = $lang->T('passDescription');
        $passwd->setDescription($description);
        $passwd->addValidator(new PasswordsValidator());
        $passwd->notEmpty();

        $repPasswd = (new Password('repeat_password'))->addValidator(new RepeatPasswordValidator());
        $repPasswd->notEmpty();
        $this->generateDoubleSection([$passwd, $repPasswd]);

        $select = new Select('max_email_size');

        $this->generateDoubleSection([$select]);

    }
}
