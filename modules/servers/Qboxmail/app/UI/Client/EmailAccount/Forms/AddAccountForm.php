<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Forms;

use ModulesGarden\Servers\Qboxmail\App\Libs\Product\ProductHelper;
use ModulesGarden\Servers\Qboxmail\App\Libs\Zimbra\Components\Api\Soap\Models\ClassOfService;
use ModulesGarden\Servers\Qboxmail\App\Libs\Zimbra\Components\Api\Soap\Repository\ClassOfServices;
use ModulesGarden\Servers\Qboxmail\App\Traits\FormExtendedTrait;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\Custom\Forms\SortedFieldForm;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\Custom\Sections\RowSection;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Providers\AccountDataProvider;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Sections\AdditionalSection;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Sections\GeneralSection;
use ModulesGarden\Servers\Qboxmail\App\Validators\PasswordsValidator;
use ModulesGarden\Servers\Qboxmail\App\Validators\RepeatPasswordValidator;
use function ModulesGarden\Servers\Qboxmail\Core\Helper\di;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\BaseForm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Hidden;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Password;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Select;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Text;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\FormConstants;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\HalfPageSection;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\InputGroup;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\InputGroupElements;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\RawSection;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 10.09.19
 * Time: 13:06
 * Class AddAccountForm
 */
class AddAccountForm extends BaseForm implements ClientArea
{
    use FormExtendedTrait;

    protected $id = 'addAccountForm';
    protected $name = 'addAccountForm';
    protected $title = 'addAccountForm';

    public function initContent()
    {
        $this->setFormType(FormConstants::CREATE);
        $this->setProvider(new AccountDataProvider());
        $this->initFields();
        $this->loadDataToForm();
    }

    public function initFields()
    {
        $this->addSection(new GeneralSection());
    }

}
