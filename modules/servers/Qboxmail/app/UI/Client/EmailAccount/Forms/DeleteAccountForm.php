<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Forms;


use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Providers\DeleteAccountDataProvider;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\BaseForm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Hidden;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\FormConstants;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 11:28
 * Class DeleteAccountForm
 */
class DeleteAccountForm extends BaseForm implements ClientArea
{
    protected $id = 'deleteAccountForm';
    protected $name = 'deleteAccountForm';
    protected $title = 'deleteAccountForm';

    public function initContent()
    {
        $this->setFormType(FormConstants::DELETE);
        $this->dataProvider = new DeleteAccountDataProvider();

        $this->setConfirmMessage('confirmRemoveAccount');

        $field = new Hidden();
        $field->setId('id');
        $field->setName('id');
        $this->addField($field);

        $this->loadDataToForm();
    }
}