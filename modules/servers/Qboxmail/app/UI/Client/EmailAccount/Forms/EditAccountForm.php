<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Forms;


use ModulesGarden\Servers\Qboxmail\App\Traits\FormExtendedTrait;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\FormConstants;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Forms\SortedFieldForm;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Sections\EditGeneralSection;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Providers\EditAccountDataProvider;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 09:29
 * Class EditAccountForm
 */
class EditAccountForm extends SortedFieldForm implements ClientArea
{
    use FormExtendedTrait;

    protected $id = 'editAccountForm';
    protected $name = 'editAccountForm';
    protected $title = 'editAccountForm';

    public function initContent()
    {
        $this->setFormType(FormConstants::UPDATE);
        $this->setProvider(new EditAccountDataProvider());
        $this->initFields();
        $this->loadDataToForm();
    }

    public function initFields()
    {
        $this->addSection(new EditGeneralSection());
    }
}