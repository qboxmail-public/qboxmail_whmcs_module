<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Pages;

use ModulesGarden\Servers\Qboxmail\Core\ServiceLocator;
use ModulesGarden\Servers\Qboxmail\Core\UI\Builder\BaseContainer;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;

class EmailAccountDescription extends BaseContainer implements ClientArea
{
    protected $id = 'emailAccountDescription';
    protected $name = 'emailAccountDescription';
    protected $title = null;

    /**
     * load buttons
     */
    public function initContent()
    {
        $lang                                                = ServiceLocator::call('lang');
        $this->customTplVars['emailAccountDescriptionTitle'] = $lang->T('emailAccountDescription');
    }
}
