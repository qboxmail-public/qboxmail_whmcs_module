<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Pages;

use ModulesGarden\Servers\Qboxmail\App\Enums\Qboxmail;
use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\DataTable\Column;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\DataTable\DataTable;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Fields\EnabledField;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Buttons\AddAccountButton;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Buttons\EditAccountButton;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Buttons\DeleteAccountButton;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\DataTable\DataProviders\Providers\ArrayDataProvider;
use function ModulesGarden\Servers\Qboxmail\Core\Helper\di;

class EmailAccount extends DataTable implements ClientArea
{
    protected $id = 'accounts';
    protected $name = 'accounts';
    protected $title = null;


    /**
     * load columns
     */
    protected function loadHtml()
    {
        $this
            ->addColumn((new Column('fullName'))
                ->setOrderable()
                ->setSearchable(true))
            ->addColumn((new Column('email'))
                ->setOrderable()
                ->setSearchable(true))
            ->addColumn((new Column('usedQuota'))
                ->setOrderable()
                ->setSearchable(true))
            ->addColumn((new Column('maxQuota'))
                ->setOrderable()
                ->setSearchable(true))
            ->addColumn((new Column('status'))
                ->setOrderable()
                ->setSearchable(true));
    }

    /**
     * load buttons
     */
    public function initContent()
    {
        $this->addButton(new AddAccountButton());
        $this->addActionButton(new EditAccountButton());
        $this->addActionButton(new DeleteAccountButton());
    }

    /**
     * load data
     */
    public function loadData()
    {
        $data = [];

        $api = new QboxmailApi();

        $result = new \stdClass();
        try {
            $result = $api->getEmails($_REQUEST['id']);
        } catch (\Exception $e) {
            $result->resources = [];
        }


        foreach ($result->resources as $key => $email) {
            $tmp = [
                'id'        => $email->code,
                'email'     => $email->email_address,
                'fullName'  => "$email->firstname $email->lastname",
                'maxQuota'  => $this->bytesToGib($email->max_email_quota),
                'usedQuota' => $this->bytesToGib($email->quota, true),
                'status'    => $email->status,
            ];

            $data[] = $tmp;
        }

        $dataProv = new ArrayDataProvider();
        $dataProv->setDefaultSorting('mailbox', 'ASC')->setData($data);
        $this->setDataProvider($dataProv);
    }

    public function replaceFieldStatus($key, $row)
    {
        $status = Qboxmail::STATUS_LABEL[$row[$key]] ?: Qboxmail::STATUS_LABEL['default'];
        $label  = di('lang')->absoluteT('account', 'status', $row[$key]);

        $field = new EnabledField();
        $field->setRawType($status);
        $field->setRawTitle($label);

        return $field->getHtml();

    }

    public function replaceFieldUsedQuota($key, $row)
    {

        if (empty($row['usedQuota'])) {
            $row['usedQuota'] = 0;
        }

        return $row['usedQuota'] . ' GiB';
    }

    public function replaceFieldMaxQuota($key, $row)
    {

        if ($row['maxQuota'] == '-1') {
            return 'Unlimited';
        }

        return $row['maxQuota'] . ' GiB';
    }


    private function bytesToGib($bytes, $round = false)
    {
        if ($round) {
            return round($bytes / 1024 / 1024 / 1024, 2);
        }

        return floor($bytes / 1024 / 1024 / 1024);
    }
}
