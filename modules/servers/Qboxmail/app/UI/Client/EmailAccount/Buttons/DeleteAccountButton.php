<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Buttons;


use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Modals\DeleteAccountModal;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Buttons\ButtonDataTableModalAction;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 11:27
 * Class DeleteAccountButton
 */
class DeleteAccountButton extends ButtonDataTableModalAction implements ClientArea
{
    protected $id = 'deleteAccountButton';
    protected $title = 'deleteAccountButton';

    public function initContent()
    {
        $this->switchToRemoveBtn();
        $this->initLoadModalAction(new DeleteAccountModal());
    }

}