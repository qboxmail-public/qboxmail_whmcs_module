<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Buttons;

use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Modals\AddAccountModal;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Buttons\ButtonCreate;

class AddAccountButton extends ButtonCreate implements ClientArea
{
    protected $id = 'addAccountButton';
    protected $title = 'addAccountButton';

    public function initContent()
    {
        $this->initLoadModalAction(new AddAccountModal());
    }
}
