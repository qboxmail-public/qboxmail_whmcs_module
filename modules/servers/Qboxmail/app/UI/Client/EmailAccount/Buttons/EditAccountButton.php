<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Buttons;


use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Modals\EditAccountModal;
use \ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Buttons\ButtonDataTableModalAction;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Modals\AddAccountModal;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Buttons\ButtonCustomAction;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Buttons\ButtonModal;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Buttons\DropdawnButtonWrappers\ButtonDropdownItem;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 09:29
 * Class EditAccountButton
 */
class EditAccountButton extends ButtonDataTableModalAction implements ClientArea
{
    protected $id = 'editAccountButton';
    protected $title = 'editAccountButton';

    public function initContent()
    {
        $this->initLoadModalAction(new EditAccountModal());
    }

}