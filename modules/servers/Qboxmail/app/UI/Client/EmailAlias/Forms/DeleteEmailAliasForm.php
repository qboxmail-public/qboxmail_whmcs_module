<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Forms;


use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Providers\DeleteEmailAliasDataProvider;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\BaseForm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Hidden;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\FormConstants;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 12:59
 * Class DeleteEmailAliasForm
 */
class DeleteEmailAliasForm extends BaseForm implements ClientArea
{
    protected $id = 'deleteAccountForm';
    protected $name = 'deleteAccountForm';
    protected $title = 'deleteAccountForm';

    public function initContent()
    {
        $this->setFormType(FormConstants::DELETE);
        $this->dataProvider = new DeleteEmailAliasDataProvider();

        $this->setConfirmMessage('confirmDeleteAccountAlias');

        $field = new Hidden('id');
        $this->addField($field);

        $this->loadDataToForm();
    }
}