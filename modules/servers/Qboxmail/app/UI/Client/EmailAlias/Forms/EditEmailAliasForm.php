<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Forms;

use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Providers\EditEmailAliasDataProvider;
use ModulesGarden\Servers\Qboxmail\App\Validators\DestinationsValidator;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Hidden;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Tagger;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\FormConstants;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Forms\SortedFieldForm;


/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 11:50
 * Class AddEmailAliasForm
 */
class EditEmailAliasForm extends SortedFieldForm implements ClientArea
{
    protected $id = 'editEmailAliasForm';
    protected $name = 'editEmailAliasForm';
    protected $title = 'editEmailAliasForm';

    public function initContent()
    {
        $this->setFormType(FormConstants::UPDATE);
        $this->setProvider(new EditEmailAliasDataProvider());
        $this->initFields();
        $this->loadDataToForm();
    }


    public function initFields()
    {
        $field = new Hidden('id');
        $this->addField($field);

        $tagger = new Tagger('destinations');
        $tagger->addValidator(new DestinationsValidator());
        $this->addField($tagger);


    }
}

