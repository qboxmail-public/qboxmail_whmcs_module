<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Forms;

use ModulesGarden\Servers\Qboxmail\App\Validators\DestinationsValidator;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Hidden;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Select;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Tagger;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\FormConstants;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\InputGroup;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Forms\SortedFieldForm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\InputGroupElements;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAccount\Providers\AccountDataProvider;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Providers\AddEmailAliasDataProvider;
use function ModulesGarden\Servers\Qboxmail\Core\Helper\di;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 11:50
 * Class AddEmailAliasForm
 */
class AddEmailAliasForm extends SortedFieldForm implements ClientArea
{
    protected $id = 'addEmailAliasForm';
    protected $name = 'addEmailAliasForm';
    protected $title = 'addEmailAliasForm';

    public function initContent()
    {
        $this->setFormType(FormConstants::CREATE);
        $this->setProvider(new AddEmailAliasDataProvider());
        $this->initFields();
        $this->loadDataToForm();
    }


    public function initFields()
    {
        $email = new InputGroup('usernameGroup');

        $tagger = new Tagger('destinations');
        $tagger->setPlaceholder(di('lang')->absoluteT('mail@example.com'));
        $tagger->addValidator(new DestinationsValidator());
        $this->addField($tagger);

        $email->addTextField('alias', false, true);
        $email->addInputAddon('emailSign', false, '@');
        $email->addInputComponent((new InputGroupElements\Text('domain'))->addHtmlAttribute('readonly', 'true'));
        $this->addSection($email);
    }
}

