<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Pages;

use ModulesGarden\Servers\Qboxmail\Core\ServiceLocator;
use ModulesGarden\Servers\Qboxmail\Core\UI\Builder\BaseContainer;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;

class EmailAliasDescription extends BaseContainer implements ClientArea
{
    protected $id = 'emailAliasDescription';
    protected $name = 'emailAliasDescription';
    protected $title = null;

    /**
     * load buttons
     */
    public function initContent()
    {
        $lang                                              = ServiceLocator::call('lang');
        $this->customTplVars['emailAliasDescriptionTitle'] = $lang->T('emailAliasDescription');
    }
}
