<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Pages;

use ModulesGarden\Servers\Qboxmail\App\Enums\Qboxmail;
use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Buttons\EditEmailAliasButton;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Fields\EnabledField;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\DataTable\Column;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\DataTable\DataTable;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Buttons\AddEmailAliasButton;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Buttons\DeleteEmailAliasButton;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\DataTable\DataProviders\Providers\ArrayDataProvider;
use function ModulesGarden\Servers\Qboxmail\Core\Helper\di;

class EmailAlias extends DataTable implements ClientArea
{

    protected $id = 'aliases';
    protected $name = 'aliases';
    protected $title = null;

    /**
     * load columns
     */
    protected function loadHtml()
    {
        $this
            ->addColumn((new Column('email'))
                ->setOrderable()
                ->setSearchable(true))
            ->addColumn((new Column('alias'))
                ->setOrderable()
                ->setSearchable(true))
            ->addColumn((new Column('status'))
                ->setOrderable()
                ->setSearchable(true));

    }

    public function initContent()
    {
        $this->addButton(new AddEmailAliasButton());
        $this->addActionButton(new EditEmailAliasButton());
        $this->addActionButton(new DeleteEmailAliasButton());
    }


    /**
     * load data
     */
    public function loadData()
    {
        $data      = [];
        $serviceId = $this->request->get('id');
        $api       = new QboxmailApi();

        $res = new \stdClass();
        try {
            $res = $api->getEmailAliases($serviceId);
        } catch (\Exception $e) {
            $res->resources = [];
        }

        foreach ($res->resources as $alias) {
            $data[] =
                [
                    'id'           => $alias->code,
                    'email'        => $alias->name . '@' . $alias->domain_name,
                    'destinations' => $alias->destinations,
                    'status'       => $alias->status,
                ];
        }

        $dataProv = new ArrayDataProvider();
        $dataProv->setDefaultSorting('mailbox', 'ASC')->setData($data);
        $this->setDataProvider($dataProv);
    }

    public function replaceFieldAlias($key, $row)
    {
        return $this->parseToHTML($row['destinations']);
    }

    public function replaceFieldStatus($key, $row)
    {
        $status = Qboxmail::STATUS_LABEL[$row[$key]] ?: Qboxmail::STATUS_LABEL['default'];
        $label  = di('lang')->absoluteT('account', 'status', $row[$key]);

        $field = new EnabledField();
        $field->setRawType($status);
        $field->setRawTitle($label);

        return $field->getHtml();

    }

    private function parseToHTML($destinations)
    {
        $str = "";
        foreach ($destinations as $dest) {
            $str .= "<li>$dest</li>";
        }

        return "<ul>$str</ul>";
    }
}
