<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Modals;


use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Forms\DeleteEmailAliasForm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Modals\BaseModal;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 12:59
 * Class DeleteEmailAliasModal
 */
class DeleteEmailAliasModal extends BaseModal implements ClientArea
{

    protected $id = 'deleteEmailAliasModal';
    protected $name = 'deleteEmailAliasModal';
    protected $title = 'deleteEmailAliasModal';

    public function initContent()
    {
        $this->setSubmitButtonClassesDanger();
        $this->setModalTitleTypeDanger();

        $this->addForm(new DeleteEmailAliasForm());
    }

}