<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Modals;

use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Forms\AddEmailAliasForm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Modals\BaseEditModal;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 11:50
 * Class AddEmailAliasModal
 */
class AddEmailAliasModal extends BaseEditModal implements ClientArea
{
    protected $id = 'addEmailAliasModal';
    protected $name = 'addEmailAliasModal';
    protected $title = 'addEmailAliasModal';

    public function initContent()
    {
        $this->addForm(new AddEmailAliasForm());
    }
}