<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Providers;

use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\Core\Models\Whmcs\Hosting;
use ModulesGarden\Servers\Qboxmail\Core\UI\ResponseTemplates\HtmlDataJsonResponse;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\DataProviders\BaseDataProvider;
use Exception;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 11:50
 * Class AddEmailAliasDataProvider
 */
class AddEmailAliasDataProvider extends BaseDataProvider
{

    public function read()
    {
        $hid = $this->request->get('id');

        $hosting              = Hosting::where('id', $hid)->first();
        $this->data['domain'] = $hosting->domain;
    }

    public function create()
    {
        try {
            $serviceId = $this->request->get('id');
            $api       = new QboxmailApi();

            $data = ['name' => $this->formData['alias'], 'destinations' => explode(',', trim($this->formData['destinations']))];
            $api->addEmailAlias($serviceId, $data);

            return (new HtmlDataJsonResponse())->setMessageAndTranslate('emailAliasHasBeenCreated')->setStatusSuccess();
        } catch (Exception $e) {
            return (new HtmlDataJsonResponse())->setMessageAndTranslate('emailAliasCreateError')->setStatusError();
        }

    }

    public function update()
    {

    }

}