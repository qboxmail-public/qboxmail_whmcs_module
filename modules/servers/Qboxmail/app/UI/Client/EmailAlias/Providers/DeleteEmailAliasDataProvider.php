<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Providers;


use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use Exception;
use ModulesGarden\Servers\Qboxmail\Core\UI\ResponseTemplates\HtmlDataJsonResponse;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\DataProviders\BaseDataProvider;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 12:59
 * Class DeleteEmailAliasDataProvider
 */
class DeleteEmailAliasDataProvider extends BaseDataProvider
{

    public function read()
    {
        $this->data['id'] = $this->actionElementId;
    }

    public function update()
    {

    }

    public function delete()
    {
        try {
            $serviceId = $this->request->get('id');
            $aliasCode = $this->formData['id'];
            $api       = new QboxmailApi();

            $api->deleteEmailAlias($serviceId, $aliasCode);

            return (new HtmlDataJsonResponse())->setMessageAndTranslate('emailAliasHasBeenDeleted')->setStatusSuccess();
        } catch (Exception $e) {
            return (new HtmlDataJsonResponse())->setMessageAndTranslate('emailAliasDeleteError')->setStatusError();
        }

    }
}