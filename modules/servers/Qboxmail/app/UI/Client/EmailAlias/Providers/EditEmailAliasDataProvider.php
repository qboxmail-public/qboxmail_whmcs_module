<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Providers;

use ModulesGarden\Servers\Qboxmail\App\Helpers\Repositories\CustomFieldRepository;
use ModulesGarden\Servers\Qboxmail\App\Libs\API\QboxmailApi;
use ModulesGarden\Servers\Qboxmail\Core\UI\ResponseTemplates\HtmlDataJsonResponse;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\DataProviders\BaseDataProvider;
use Exception;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 11:50
 * Class AddEmailAliasDataProvider
 */
class EditEmailAliasDataProvider extends BaseDataProvider
{

    public function read()
    {
        $this->data['id']           = $this->actionElementId;
        $serviceId                  = $this->request->get('id');
        $api                        = new QboxmailApi();
        $result                     = $api->getEmailAlias($serviceId, $this->actionElementId);
        $this->data['destinations'] = implode(',', $result->resources[0]->destinations);
    }


    public function update()
    {
        try {
            $serviceId      = $this->getRequestValue('id');
            $emailAliasCode = $this->formData['id'];
            $api            = new QboxmailApi();

            $api->editEmailAlias($serviceId, $emailAliasCode, ['destinations' => explode(',', $this->formData['destinations'])]);


            return (new HtmlDataJsonResponse())->setMessageAndTranslate('emailAliasHasBeenEdited')->setStatusSuccess();
        } catch (Exception $e) {
            return (new HtmlDataJsonResponse())->setMessageAndTranslate('emailAliasEditError')->setStatusError();
        }


    }
}