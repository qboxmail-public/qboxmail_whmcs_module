<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Buttons;


use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Modals\DeleteEmailAliasModal;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Buttons\ButtonDataTableModalAction;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 12:59
 * Class DeleteEmailAliasButton
 */
class DeleteEmailAliasButton extends ButtonDataTableModalAction implements ClientArea
{
    protected $id = 'deleteEmailAliasButton';
    protected $title = 'deleteEmailAliasButton';

    public function initContent()
    {
        $this->switchToRemoveBtn();
        $this->initLoadModalAction(new DeleteEmailAliasModal());
    }

}