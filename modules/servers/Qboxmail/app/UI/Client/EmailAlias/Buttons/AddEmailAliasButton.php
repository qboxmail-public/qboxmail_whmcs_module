<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Buttons;

use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Modals\AddEmailAliasModal;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Buttons\ButtonCreate;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 11:50
 * Class AddEmailAliasButton
 */
class AddEmailAliasButton extends ButtonCreate implements ClientArea
{
    protected $id = 'addEmailAliasButton';
    protected $title = 'addEmailAliasButton';

    public function initContent()
    {
        $this->initLoadModalAction(new AddEmailAliasModal());
    }


}