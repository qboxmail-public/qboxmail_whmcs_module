<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Buttons;

use ModulesGarden\Servers\Qboxmail\App\UI\Client\EmailAlias\Modals\EditEmailAliasModal;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Buttons\ButtonDataTableModalAction;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 18.09.19
 * Time: 11:50
 * Class EmailAliasButton
 */
class EditEmailAliasButton extends ButtonDataTableModalAction implements ClientArea
{
    protected $id = 'editEmailAliasButton';
    protected $title = 'editEmailAliasButton';

    public function initContent()
    {
        $this->initLoadModalAction(new EditEmailAliasModal());
    }


}