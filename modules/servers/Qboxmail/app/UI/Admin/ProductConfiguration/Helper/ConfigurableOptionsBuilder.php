<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Helper;

use ModulesGarden\Servers\Qboxmail\App\Enums\Qboxmail;
use ModulesGarden\Servers\Qboxmail\App\Service\ConfigurableOptions\ConfigurableOptions;
use ModulesGarden\Servers\Qboxmail\App\Service\ConfigurableOptions\Helper\TypeConstans;
use ModulesGarden\Servers\Qboxmail\App\Service\ConfigurableOptions\Models\Option;
use ModulesGarden\Servers\Qboxmail\App\Service\ConfigurableOptions\Models\SubOption;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Providers\ProductConfigurationDataProvider;

class ConfigurableOptionsBuilder
{

    public static function build(ConfigurableOptions $configurableOptions, $fieldsStatus = [])
    {
        foreach ($fieldsStatus as $key => $field) {
            if ($field == "on") {
                $configurableOptions->addOption(self::$key());
            }
        }

        return $configurableOptions;
    }

    public static function buildAll(ConfigurableOptions $configurableOptions)
    {
        $configurableOptions
            ->addOption(self::email_accounts_8gb())
            ->addOption(self::email_accounts_25gb())
            ->addOption(self::email_accounts_50gb())
            ->addOption(self::email_accounts_100gb());

        return $configurableOptions;
    }

    private static function email_accounts_8gb()
    {
        $option = new Option(Qboxmail::EMAIL_ACCOUNTS_8GB, 'Email Accounts 8GB', TypeConstans::QUANTITY);
        $option->addSubOption(new SubOption('1', 'Email Accounts 8GB'));

        return $option;
    }

    private static function email_accounts_25gb()
    {
        $option = new Option(Qboxmail::EMAIL_ACCOUNTS_25GB, 'Email Accounts 25GB', TypeConstans::QUANTITY);
        $option->addSubOption(new SubOption('1', 'Email Accounts 25GB'));

        return $option;
    }

    private static function email_accounts_50gb()
    {
        $option = new Option(Qboxmail::EMAIL_ACCOUNTS_50GB, 'Email Accounts 50GB', TypeConstans::QUANTITY);
        $option->addSubOption(new SubOption('1', 'Email Accounts 50GB'));

        return $option;
    }

    private static function email_accounts_100gb()
    {
        $option = new Option(Qboxmail::EMAIL_ACCOUNTS_100GB, 'Email Accounts 100GB', TypeConstans::QUANTITY);
        $option->addSubOption(new SubOption('1', 'Email Accounts 100GB'));

        return $option;
    }
}
