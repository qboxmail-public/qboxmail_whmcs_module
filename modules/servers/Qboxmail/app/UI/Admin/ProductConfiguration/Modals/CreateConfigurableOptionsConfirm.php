<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Modals;

use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Forms\CreateConfigurableForm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\AdminArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Modals\BaseEditModal;

/**
 * Class CreateConfigurableOptionsConfirm
 * User: Nessandro
 * Date: 2019-09-29
 * Time: 15:30
 */
class CreateConfigurableOptionsConfirm extends BaseEditModal implements AdminArea
{
    protected $id = 'createCOConfirmModal';
    protected $name = 'createCOConfirmModal';
    protected $title = 'createCOConfirmModal';

    public function initContent()
    {
        $this->setModalSizeLarge();
        $this->addForm(new CreateConfigurableForm());

    }

}