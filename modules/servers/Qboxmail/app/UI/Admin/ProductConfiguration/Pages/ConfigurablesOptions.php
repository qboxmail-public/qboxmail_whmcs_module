<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Pages;

use ModulesGarden\Servers\Qboxmail\App\Enums\Size;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\AdminArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Builder\BaseContainer;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Text;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Select;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Switcher;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\BoxSection;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\HalfPageSection;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Fields\ExtendedInputField;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Sections\BoxSectionExtended;
use ModulesGarden\Servers\Qboxmail\App\Service\ConfigurableOptions\ConfigurableOptions;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Helper\ConfigurableOptionsBuilder;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Buttons\CreateConfigurableOptionsBaseModalButton;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 28.08.19
 * Time: 09:15
 * Class ZimbraSettingsSection
 */
class ConfigurablesOptions extends BaseContainer implements AdminArea
{
    protected $id = 'configurablesOptions';
    protected $name = 'configurablesOptions';
    protected $title = 'Configurable Options';

    public function initContent()
    {
        $this->addButton(new CreateConfigurableOptionsBaseModalButton());
    }

    public function getOptions()
    {
        $configurableOptions = new ConfigurableOptions($_REQUEST['id']);
        ConfigurableOptionsBuilder::buildAll($configurableOptions);

        $fields                = $configurableOptions->show();
        $fields['emptyFields'] = "";

        return $fields;
    }
}