<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Pages\Sections;

use ModulesGarden\Servers\Qboxmail\App\Enums\Qboxmail;
use ModulesGarden\Servers\Qboxmail\App\Enums\Size;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Pages\Description;
use ModulesGarden\Servers\Qboxmail\Core\UI\Helpers\AlertTypesConstants;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\AdminArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Text;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Select;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Switcher;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\BaseSection;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\BoxSection;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Sections\HalfPageSection;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Fields\ExtendedInputField;
use ModulesGarden\Servers\Qboxmail\App\UI\Client\Others\Sections\BoxSectionExtended;
use function ModulesGarden\Servers\Qboxmail\Core\Helper\di;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 28.08.19
 * Time: 09:15
 * Class ZimbraSettingsSection
 */
class ProductSettings extends BoxSectionExtended implements AdminArea
{
    protected $id = 'qboxmailSettings';
    protected $name = 'qboxmailSettings';
    protected $title = 'qboxmailSettings';

    public function initContent()
    {
        $this
            ->addSection($this->getLeftSection())
            ->addSection($this->getRightSection());
    }

    /**
     * @description left section
     * @return HalfPageSection
     */
    public function getLeftSection()
    {
        $leftSection = new HalfPageSection('leftSide');

        $field = new Select(Qboxmail::PLAN);
        $field->setDescription('The plan of the domain.');
        $leftSection->addField($field);

        return $leftSection;

    }

    /**
     * @description right section
     * @return HalfPageSection
     */
    public function getRightSection()
    {
        $right = new HalfPageSection('rightSide');

        return $right;
    }
}