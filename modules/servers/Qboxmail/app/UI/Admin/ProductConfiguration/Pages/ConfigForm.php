<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Pages;

use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Pages\Sections\Description;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\AdminArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\FormIntegration;
use ModulesGarden\Servers\Qboxmail\App\Services\ConfigurableOptions\ConfigurableOptions;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Pages\Sections\ProductSettings;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Pages\Sections\ConfigurablesOptions;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Providers\ProductConfigurationDataProvider;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 28.08.19
 * Time: 09:12
 * Class ConfigForm
 */
class ConfigForm extends FormIntegration implements AdminArea
{
    protected $id = 'configForm';
    protected $name = 'configForm';
    protected $title = 'configForm';

    public function initContent()
    {
        /** add data provider **/
        $this->setProvider(new ProductConfigurationDataProvider());
        $this->addSection(new ProductSettings());

        $this->loadDataToForm();
    }

}