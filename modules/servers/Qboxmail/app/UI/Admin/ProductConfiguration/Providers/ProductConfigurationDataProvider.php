<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Providers;


use ModulesGarden\Servers\Qboxmail\App\Enums\Qboxmail;
use ModulesGarden\Servers\Qboxmail\Core\App\Controllers\Interfaces\AdminArea;
use ModulesGarden\Servers\Qboxmail\App\Models\ProductConfiguration as ProductConfigModel;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\DataProviders\BaseDataProvider;
use function ModulesGarden\Servers\Qboxmail\Core\Helper\di;

/**
 *
 * Created by PhpStorm.
 * User: Tomasz Bielecki ( tomasz.bi@modulesgarden.com )
 * Date: 29.08.19
 * Time: 10:27
 * Class ProductConfigurationDataProvider
 */
class ProductConfigurationDataProvider extends BaseDataProvider implements AdminArea
{
    const FORM_DATA = [
        Qboxmail::PLAN,
    ];


    const QBOXMAIL_PLAN = [
        'Basic'        => 'basic',
        "Professional" => 'professional',
        "Enterprise"   => 'enterprise',
    ];

    const QBOXMAIL_QUOTA = [
        '1 GiB'   => 1,
        '2 GiB'   => 2,
        '3 GiB'   => 3,
        '4 GiB'   => 4,
        '5 GiB'   => 5,
        '6 GiB'   => 6,
        '7 GiB'   => 7,
        '8 GiB'   => 8,
        '16 GiB'  => 16,
        '25 GiB'  => 25,
        '50 GiB'  => 50,
        '100 GiB' => 100,
    ];

    protected function catchFormData()
    {
        /**
         * catch from request
         */
        $params = self::FORM_DATA;
        foreach ($params as $name) {
            $value = $this->request->get($name);
            if ($value) {
                $this->formData[$name] = $value;
            } else {
                $this->formData[$name] = '';
            }
        }
    }

    public function read()
    {
        try {
            $settings = ProductConfigModel::where('product_id', $this->request->get('id'))->get();

            foreach (self::QBOXMAIL_PLAN as $access) {
                $this->availableValues[Qboxmail::PLAN][$access] = di('lang')->absoluteT('plan', $access, $access);
            }

            foreach ($settings as $setting) {
                $this->data[$setting->setting] = $setting->value;
            }
        } catch (\Exception $e) {
            logActivity($e->getMessage());
        }


    }

    public function update()
    {
        $this->catchFormData();
        $productId = $this->request->get('id');


        foreach ($this->formData as $key => $value) {
            ProductConfigModel::updateOrCreate(['product_id' => $productId, 'setting' => $key], ['value' => $value]);
        }
    }
}