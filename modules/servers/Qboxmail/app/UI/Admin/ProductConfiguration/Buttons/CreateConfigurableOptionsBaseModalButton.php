<?php
namespace ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Buttons;

use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Modals\CreateConfigurableOptionsConfirm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\AdminArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Buttons\ButtonCreate;

/**
 * Class CreateConfigurableOptionsBaseModalButton
 * User: Nessandro
 * Date: 2019-09-29
 * Time: 15:29
 */
class CreateConfigurableOptionsBaseModalButton extends ButtonCreate implements AdminArea
{
    protected $id = 'createCOBaseModalButton';
    protected $name = 'createCOBaseModalButton';
    protected $title = 'createCOBaseModalButton';
    protected $class = ['lu-btn lu-btn--success'];
    protected $htmlAttributes = [
        'href' => 'javascript:;',
    ];

    public function initContent()
    {

        $this->initLoadModalAction(new CreateConfigurableOptionsConfirm());
    }

}