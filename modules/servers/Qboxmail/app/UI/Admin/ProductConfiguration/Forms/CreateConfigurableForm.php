<?php

namespace ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Forms;

use function ModulesGarden\Servers\Qboxmail\Core\Helper\di;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\AdminArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Interfaces\ClientArea;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\BaseForm;
use ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\FormConstants;
use ModulesGarden\Servers\Qboxmail\Core\UI\Helpers\AlertTypesConstants;
use ModulesGarden\Servers\Qboxmail\App\UI\Admin\ProductConfiguration\Providers\ConfigurableOptionManage;

class CreateConfigurableForm extends BaseForm implements ClientArea, AdminArea
{

    protected $id = 'createConfigurableForm';
    protected $name = 'createConfigurableForm';
    protected $title = 'createConfigurableForm';

    public function getClass()
    {

    }

    public function initContent()
    {
        $lang = di('lang');
        $lang->addReplacementConstant('configurableOptionsNameUrl',
            '<a style="color: #31708f; text-decoration: underline;" href="https://docs.whmcs.com/Addons_and_Configurable_Options" target="blank">here</a>');

        $this->addInternalAlert($lang->absoluteT('configurableOptionsWhmcsInfo'), AlertTypesConstants::INFO,
            AlertTypesConstants::SMALL);
        $this->setFormType(FormConstants::CREATE);
        $this->setProvider(new ConfigurableOptionManage());
        $this->loadDataToForm();
        $dataProvider = $this->getFormData();

        if (is_array($dataProvider['fields'])) {
            foreach ($dataProvider['fields'] as $key => $name) {
                $this->addField((new \ModulesGarden\Servers\Qboxmail\Core\UI\Widget\Forms\Fields\Switcher($key))->setDefaultValue('on')->setRawTitle($key . '|' . $name));
            }
        }

    }

}
