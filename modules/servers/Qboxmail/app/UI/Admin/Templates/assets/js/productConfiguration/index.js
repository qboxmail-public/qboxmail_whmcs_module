//replace url wrapper
var mgUrlParser = {
    oldMgUrlParser: mgUrlParser,

    getCurrentUrl: function () {
        var url = this.oldMgUrlParser.getCurrentUrl();
        return url.replace("action=edit", "action=module-settings").replace("&success=true", "");
    }
};


function redirectToPage(tabNumber) {
    var windowAddress = String(window.location);
    var hashTab = windowAddress.indexOf('#tab=');
    var andTab = windowAddress.indexOf('&tab=');
    if (hashTab > 0 && andTab > 0) {
        var count = Math.min(hashTab, andTab);
    } else {
        var count = Math.max(hashTab, andTab);
    }

    var redirectTo = (count > 0) ? windowAddress.substring(0, count) : windowAddress;
    window.location = redirectTo + '&tab=' + tabNumber;
}

function redirectToConfigurableOptionsTab() {
    redirectToPage(5);
}

function redirectToCustomFieldsTab() {
    redirectToPage(4);

}

function productConfigurationSelect(event) {
    productConfigurationSettings(event, event.target.value);
}

function productConfigurationSettings(event, packageName) {
}

$(document).ready(function () {
    //Fix Bug
    let el = $('.lu-widget__content.configOptionBox .lu-row').children().last();
    if (!el.html().trim()) {
        el.remove();
    }
    handleInputs();
})

$(document).on('change', 'select[name="plan"]', function () {
    handleInputs();
});

function handleInputs() {
    // let currentValue = $('select[name="plan"]').val();
    //
    // $('input[name="max_email_accounts_50gb"]').prop('disabled', true);
    // $('input[name="max_email_accounts_100gb"]').prop('disabled', true);
    //
    // if (currentValue === 'basic') {
    //     $('input[name="max_email_accounts_50gb"]').val(0)
    //     $('input[name="max_email_accounts_100gb"]').val(0)
    // } else if (currentValue === 'professional') {
    //     $('input[name="max_email_accounts_50gb"]').prop('disabled', false);
    //     $('input[name="max_email_accounts_100gb"]').val(0)
    // } else if (currentValue === 'enterprise') {
    //     $('input[name="max_email_accounts_50gb"]').prop('disabled', false);
    //     $('input[name="max_email_accounts_100gb"]').prop('disabled', false);
    // }

}