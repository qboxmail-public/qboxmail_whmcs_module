<?php

use \ModulesGarden\Servers\Qboxmail\Core\App\AppContext;

if ( ! defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

if ( ! defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'WhmcsErrorIntegration.php';


#MGLICENSE_FUNCTIONS#


function Qboxmail_CreateAccount(array $params)
{
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'AppContext.php';

    #MGLICENSE_CHECK_RETURN#

    $appContext = new AppContext();

    return $appContext->runApp(__FUNCTION__, $params);
}

function Qboxmail_SuspendAccount(array $params)
{
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'AppContext.php';

    #MGLICENSE_CHECK_RETURN#

    $appContext = new AppContext();

    return $appContext->runApp(__FUNCTION__, $params);
}

function Qboxmail_UnsuspendAccount(array $params)
{
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'AppContext.php';

    #MGLICENSE_CHECK_RETURN#

    $appContext = new AppContext();

    return $appContext->runApp(__FUNCTION__, $params);
}

function Qboxmail_TerminateAccount(array $params)
{
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'AppContext.php';

    #MGLICENSE_CHECK_RETURN#

    $appContext = new AppContext();

    return $appContext->runApp(__FUNCTION__, $params);
}

function Qboxmail_ChangePackage(array $params)
{
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'AppContext.php';

    #MGLICENSE_CHECK_RETURN#

    $appContext = new AppContext();

    return $appContext->runApp(__FUNCTION__, $params);
}

function Qboxmail_TestConnection(array $params)
{
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'AppContext.php';

    $appContext = new AppContext();

    return $appContext->runApp(__FUNCTION__, $params);
}

function Qboxmail_ConfigOptions(array $params)
{
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'AppContext.php';

    $appContext = new AppContext();

    return $appContext->runApp(__FUNCTION__, $params);
}

function Qboxmail_MetaData()
{
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'AppContext.php';

    $appContext = new AppContext();

    return $appContext->runApp(__FUNCTION__);
}

function Qboxmail_AdminServicesTabFields($params)
{
    $_SESSION['serviceid'] = $params['serviceid'];

    require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'AppContext.php';

    $appContext = new AppContext();

    return $appContext->runApp(__FUNCTION__, $params);
}

function Qboxmail_ClientArea($params)
{
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'AppContext.php';

    #MGLICENSE_CHECK_RETURN#

    $appContext = new AppContext();
    if (isset($_REQUEST['mg-page'])) {
        return $appContext->runApp('clientarea', $params);
    }
    if ( ! isset($_REQUEST['mg-page']) && ! isset($_REQUEST['modop'])) {
        return $appContext->runApp('clientarea', $params);
    }
}
